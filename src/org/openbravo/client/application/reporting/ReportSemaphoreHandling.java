/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.client.application.reporting;

import org.openbravo.base.exception.OBException;

/**
 * A class which can be used to limit the number of parallel processes running. If no semaphore is
 * available an {@link OBException} is thrown.
 * 
 * This semaphore handler can/should be used by heavy resource intensive reporting processes.To
 * prevent too many to run at the same time.
 * 
 * The {@link #acquire()} and {@link #release()} methods should be called using a try finally block:
 * 
 * call acquire before the try:
 * 
 * ReportSemaphoreHandling.acquire();
 * 
 * try {
 * 
 * } finally {
 * 
 * ReportSemaphoreHandling.release();
 * 
 * }
 * 
 * @author mtaal
 */
public class ReportSemaphoreHandling {

  private static final int MAX_THREADS = 3;

  private static int threadCounter = 0;

  public static synchronized void acquire() {
    if (threadCounter == MAX_THREADS) {
      throw new OBException("OBUIREP_ReportProcessOccupied");
    }
    threadCounter++;
  }

  public static synchronized void release() {
    if (threadCounter == 0) {
      throw new OBException("Illegal thread counter, decreasing below zero");
    }
    threadCounter--;
  }
}
