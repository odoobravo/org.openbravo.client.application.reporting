/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.client.application.reporting;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;
import org.apache.poi.ss.formula.FormulaType;
import org.apache.poi.ss.formula.ptg.AreaPtgBase;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.RefPtgBase;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.application.Process;
import org.openbravo.client.kernel.reference.UIDefinitionController;
import org.openbravo.client.kernel.reference.UIDefinitionController.FormatDefinition;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.JRFormatFactory;
import org.openbravo.model.common.currency.Currency;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Utilities for reporting.
 * 
 * @author mtaal
 */
public class ReportingUtils {

  private static final long ONE_SECOND = 1000;
  private static final long ONE_DAY = 24 * 60 * 60 * ONE_SECOND;

  public static final BigDecimal ZERO = new BigDecimal("0.0");
  public static final BigDecimal MINUS_ONE = new BigDecimal("-1.0");
  public static final String JASPER_PARAM_OBCONTEXT = "jasper_obContext";
  public static final String JASPER_PARAM_HBSESSION = "jasper_hbSession";
  public static final String JASPER_PARAM_PROCESS = "jasper_process";

  public static String formatCurrency(Object currencyObject, BigDecimal value,
      Object decimalFormatObject) {
    final Currency currency = (Currency) currencyObject;
    final DecimalFormat decimalFormat = (DecimalFormat) decimalFormatObject;
    final boolean negative = value.compareTo(ZERO) < 0;
    final BigDecimal useValue;
    if (negative) {
      useValue = value.multiply(MINUS_ONE);
    } else {
      useValue = value;
    }
    String strValue = decimalFormat.format(useValue);
    if (currency != null) {
      if (currency.isCurrencySymbolAtTheRight()) {
        strValue = strValue + " " + currency.getSymbol();
      } else {
        strValue = currency.getSymbol() + " " + strValue;
      }
    }
    if (negative) {
      return "(" + strValue + ")";
    }
    return strValue;
  }

  public static String formatNumber(BigDecimal value, Object decimalFormatObject) {
    final DecimalFormat decimalFormat = (DecimalFormat) decimalFormatObject;
    final boolean negative = value.compareTo(ZERO) < 0;
    final BigDecimal useValue;
    if (negative) {
      useValue = value.multiply(MINUS_ONE);
    } else {
      useValue = value;
    }
    String strValue = decimalFormat.format(useValue);
    if (negative) {
      return "(" + strValue + ")";
    }
    return strValue;
  }

  /**
   * if dateFrom.getTime() == dateTo.getTime() then a new date is returned which is 24 hours minus
   * one second later than dateFrom. This to make sure that at least one day of information can be
   * retrieved.
   */
  public static Date correctToDate(Date dateFrom, Date dateTo) {
    if (dateFrom == null || dateTo == null) {
      return dateTo;
    }
    if (dateFrom.getTime() != dateTo.getTime()) {
      return dateTo;
    }
    return new Date(dateTo.getTime() + ONE_DAY - ONE_SECOND);
  }

  public static void exportToPDF(Process process, Map<String, Object> parameters, OutputStream os,
      String jasperFilePath) {
    try {
      parameters.put(JASPER_PARAM_HBSESSION, OBDal.getInstance().getSession());
      parameters.put(JASPER_PARAM_OBCONTEXT, OBContext.getOBContext());
      parameters.put(JASPER_PARAM_PROCESS, process);
      parameters.put("IS_IGNORE_PAGINATION", false);

      {
        final FormatDefinition reportFormat = UIDefinitionController.getInstance()
            .getFormatDefinition("amount", UIDefinitionController.NORMALFORMAT_QUALIFIER);

        final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(reportFormat.getDecimalSymbol().charAt(0));
        dfs.setGroupingSeparator(reportFormat.getGroupingSymbol().charAt(0));

        final DecimalFormat numberFormat = new DecimalFormat(correctMaskForGrouping(
            reportFormat.getFormat(), reportFormat.getDecimalSymbol(),
            reportFormat.getGroupingSymbol()), dfs);
        parameters.put("AMOUNTFORMAT", numberFormat);
      }

      {
        final FormatDefinition reportFormat = UIDefinitionController.getInstance()
            .getFormatDefinition("generalQty", UIDefinitionController.SHORTFORMAT_QUALIFIER);

        final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(reportFormat.getDecimalSymbol().charAt(0));
        dfs.setGroupingSeparator(reportFormat.getGroupingSymbol().charAt(0));

        final DecimalFormat numberFormat = new DecimalFormat(correctMaskForGrouping(
            reportFormat.getFormat(), reportFormat.getDecimalSymbol(),
            reportFormat.getGroupingSymbol()), dfs);
        parameters.put("QUANTITYFORMAT", numberFormat);
      }

      final JRFormatFactory jrFormatFactory = new JRFormatFactory();
      jrFormatFactory.setDatePattern((OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("dateFormat.java")));
      parameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);

      final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFilePath, parameters,
          new JREmptyDataSource(1));
      JasperExportManager.exportReportToPdfStream(jasperPrint, os);

    } catch (Exception e) {
      throw new OBException("Error exporting for process: " + process.getName() + " to pdf", e);
    }
  }

  private static String correctMaskForGrouping(String mask, String decimalSymbol,
      String groupingSymbol) {
    String localMask = mask.replace(decimalSymbol, "_");
    localMask = localMask.replace(groupingSymbol, ",");
    return localMask.replaceAll("_", ".");
  }

  public static SortedMap<String, List<Map<String, Object>>> mapAndSortByGroup(
      String groupPropName, String recordSortProperty, List<Map<String, Object>> data) {
    SortedMap<String, List<Map<String, Object>>> result = new TreeMap<String, List<Map<String, Object>>>();
    for (Map<String, Object> record : data) {
      final String group = (String) record.get(groupPropName);
      List<Map<String, Object>> groupData = result.get(group);
      if (groupData == null) {
        groupData = new ArrayList<Map<String, Object>>();
        result.put(group, groupData);
      }
      groupData.add(record);
    }

    // sort the records within the group
    final StringDataComparator comparator = new StringDataComparator(recordSortProperty);
    for (List<Map<String, Object>> records : result.values()) {
      Collections.sort(records, comparator);
    }

    return result;
  }

  public static XSSFSheet getSheet(XSSFWorkbook workBook, String sheetName) {
    final String safeSheetName = WorkbookUtil.createSafeSheetName(sheetName);
    XSSFSheet workSheet = workBook.getSheet(safeSheetName);
    if (workSheet == null) {
      workSheet = workBook.createSheet(safeSheetName);
    }
    return workSheet;
  }

  public static void copyRow(XSSFSheet workSheet, int fromRow, int toRow) {
    int offset = fromRow > toRow ? 1 : 0;
    // Get the source / new row
    XSSFRow newRow = workSheet.getRow(toRow);

    // If the row exist in destination, push down all rows by 1 else create a new row
    if (newRow != null) {
      final int totalRows = workSheet.getLastRowNum();
      workSheet.shiftRows(toRow, totalRows, 1, true, true);
    }
    newRow = workSheet.createRow(toRow);
    XSSFRow sourceRow = workSheet.getRow(fromRow + offset);

    // Loop through source columns to add to new row
    for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
      // Grab a copy of the old/new cell
      XSSFCell oldCell = sourceRow.getCell(i);
      XSSFCell newCell = newRow.createCell(i);

      // If the old cell is null jump to next cell
      if (oldCell == null) {
        newCell = null;
        continue;
      }

      // Use old cell style
      newCell.setCellStyle(oldCell.getCellStyle());

      // If there is a cell hyperlink, copy
      if (oldCell.getHyperlink() != null) {
        newCell.setHyperlink(oldCell.getHyperlink());
      }

      // Set the cell data type
      newCell.setCellType(oldCell.getCellType());

      // Set the cell data value
      switch (oldCell.getCellType()) {
      case Cell.CELL_TYPE_BLANK:
        newCell.setCellValue(oldCell.getStringCellValue());
        break;
      case Cell.CELL_TYPE_BOOLEAN:
        newCell.setCellValue(oldCell.getBooleanCellValue());
        break;
      case Cell.CELL_TYPE_ERROR:
        newCell.setCellErrorValue(oldCell.getErrorCellValue());
        break;
      case Cell.CELL_TYPE_FORMULA:
        newCell
            .setCellFormula(getCopyFormula(workSheet.getWorkbook(), workSheet, oldCell, newCell));
        break;
      case Cell.CELL_TYPE_NUMERIC:
        newCell.setCellValue(oldCell.getNumericCellValue());
        break;
      case Cell.CELL_TYPE_STRING:
        newCell.setCellValue(oldCell.getRichStringCellValue());
        break;
      }
    }

    // If there are are any merged regions in the source row, copy to new row
    for (int i = 0; i < workSheet.getNumMergedRegions(); i++) {
      CellRangeAddress cellRangeAddress = workSheet.getMergedRegion(i);
      if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
        CellRangeAddress newCellRangeAddress = new CellRangeAddress(
            newRow.getRowNum(),
            (newRow.getRowNum() + (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow())),
            cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn());
        workSheet.addMergedRegion(newCellRangeAddress);
      }
    }
  }

  public static void setCellValue(XSSFSheet workSheet, int col, int row, Object value) {
    XSSFRow newRow = workSheet.getRow(row);
    setCellValue(workSheet, newRow, col, value);
  }

  public static void setCellValue(XSSFSheet workSheet, Row row, int col, Object value) {

    if (value == null) {
      return;
    }
    // final Row row = workSheet.createRow((short) y + startRow);
    Cell cell = row.getCell(col);
    if (cell == null) {
      cell = row.createCell(col);
    }
    if (value instanceof BaseOBObject) {
      cell.setCellValue(((BaseOBObject) value).getIdentifier());
    } else if (value instanceof Number) {
      cell.setCellValue(((Number) value).doubleValue());
    } else if (value instanceof Timestamp) {
      final XSSFCellStyle cellStyle = workSheet.getWorkbook().createCellStyle();
      cellStyle.cloneStyleFrom(cell.getCellStyle());
      final DataFormat df = workSheet.getWorkbook().createDataFormat();
      cellStyle.setDataFormat(df.getFormat(OBPropertiesProvider.getInstance()
          .getOpenbravoProperties().getProperty("dateTimeFormat.java")));
      cell.setCellStyle(cellStyle);
      cell.setCellValue((Date) value);
    } else if (value instanceof Date) {
      final XSSFCellStyle cellStyle = workSheet.getWorkbook().createCellStyle();
      cellStyle.cloneStyleFrom(cell.getCellStyle());
      final DataFormat df = workSheet.getWorkbook().createDataFormat();
      cellStyle.setDataFormat(df.getFormat(OBPropertiesProvider.getInstance()
          .getOpenbravoProperties().getProperty("dateFormat.java")));
      cell.setCellStyle(cellStyle);

      cell.setCellValue((Date) value);
    } else if (value instanceof Boolean) {
      cell.setCellValue((Boolean) value);
    } else if (value instanceof CellFormula) {
      cell.setCellType(XSSFCell.CELL_TYPE_FORMULA);
      cell.setCellFormula(((CellFormula) value).getFormula());
    } else {
      cell.setCellValue(value.toString());
    }
  }

  public static void addDataToSummaryRecord(Map<String, Object> summaryRecord,
      Map<String, Object> record) {
    for (String key : record.keySet()) {
      final Object value = record.get(key);
      Object targetValue = summaryRecord.get(key);
      if (value == null) {
        continue;
      } else if (targetValue == null) {
        summaryRecord.put(key, targetValue);
      } else if (targetValue.getClass() != value.getClass()) {
        throw new OBException("Sum action no possible, values have different classes "
            + targetValue + " sourceValue " + value);
      }
      if (value instanceof Integer) {
        summaryRecord.put(key, (Integer) ((Integer) targetValue) + ((Integer) value));
      }
      if (value instanceof Double) {
        summaryRecord.put(key, (Double) ((Double) targetValue) + ((Double) value));
      }
      if (value instanceof BigDecimal) {
        summaryRecord.put(key, ((BigDecimal) targetValue).add((BigDecimal) value));
      }
      if (value instanceof BigInteger) {
        summaryRecord.put(key, ((BigInteger) targetValue).add((BigInteger) value));
      }
    }
  }

  public static class StringDataComparator implements Comparator<Map<String, Object>> {
    private final String[] propNames;

    public StringDataComparator(String... propNames) {
      this.propNames = propNames;
    }

    @Override
    public int compare(Map<String, Object> m1, Map<String, Object> m2) {
      for (String propName : propNames) {
        final int res = compareValue(m1, m2, propName);
        if (res != 0) {
          return res;
        }
      }
      return 0;
    }

    private int compareValue(Map<String, Object> m1, Map<String, Object> m2, String propName) {
      final String s1 = (String) m1.get(propName);
      final String s2 = (String) m2.get(propName);
      if (s1 == null && s2 == null) {
        return 0;
      }
      if (s1 != null && s2 == null) {
        return 1;
      }
      if (s1 == null && s2 != null) {
        return -1;
      }
      return s1.compareTo(s2);
    }
  }

  public static class CellFormula {
    private final String formula;

    public CellFormula(String formula) {
      this.formula = formula;
    }

    public String getFormula() {
      return formula;
    }

  }

  private static String getCopyFormula(Workbook workbook, Sheet sheet, Cell oldCell, Cell newCell) {
    String oldFormula = oldCell.getCellFormula();
    String newFormula = new String();

    if (oldFormula != null) {
      FormulaParsingWorkbook parsingWorkbook = null;
      FormulaRenderingWorkbook renderingWorkbook = null;

      if (workbook instanceof XSSFWorkbook) {
        parsingWorkbook = XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);
        renderingWorkbook = XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);
      } else if (workbook instanceof XSSFWorkbook) {
        parsingWorkbook = XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);
        renderingWorkbook = XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);
      }

      // get PTG's in the formula
      Ptg[] ptgs = FormulaParser.parse(oldFormula, parsingWorkbook, FormulaType.CELL,
          workbook.getSheetIndex(sheet));

      // iterating through all PTG's
      for (Ptg ptg : ptgs) {
        if (ptg instanceof RefPtgBase) // for references such as A1, A2, B3
        {
          RefPtgBase refPtgBase = (RefPtgBase) ptg;

          // if row is relative
          if (refPtgBase.isRowRelative()) {
            refPtgBase.setRow((short) (newCell.getRowIndex() - (oldCell.getRowIndex() - refPtgBase
                .getRow())));
          }

          // if col is relative
          if (refPtgBase.isColRelative()) {
            refPtgBase
                .setColumn((short) (newCell.getColumnIndex() - (oldCell.getColumnIndex() - refPtgBase
                    .getColumn())));
          }
        }
        if (ptg instanceof AreaPtgBase) // for area of cells A1:A4
        {
          AreaPtgBase areaPtgBase = (AreaPtgBase) ptg;

          // if first row is relative
          if (areaPtgBase.isFirstRowRelative()) {
            areaPtgBase
                .setFirstRow((short) (newCell.getRowIndex() - (oldCell.getRowIndex() - areaPtgBase
                    .getFirstRow())));
          }

          // if last row is relative
          if (areaPtgBase.isLastRowRelative()) {
            areaPtgBase
                .setLastRow((short) (newCell.getRowIndex() - (oldCell.getRowIndex() - areaPtgBase
                    .getLastRow())));
          }

          // if first column is relative
          if (areaPtgBase.isFirstColRelative()) {
            areaPtgBase.setFirstColumn((short) (newCell.getColumnIndex() - (oldCell
                .getColumnIndex() - areaPtgBase.getFirstColumn())));
          }

          // if last column is relative
          if (areaPtgBase.isLastColRelative()) {
            areaPtgBase
                .setLastColumn((short) (newCell.getColumnIndex() - (oldCell.getColumnIndex() - areaPtgBase
                    .getLastColumn())));
          }
        }
      }

      newFormula = FormulaRenderer.toFormulaString(renderingWorkbook, ptgs);
    }

    return newFormula;
  }

  public static class SumRangeCellFormula extends CellFormula {

    public SumRangeCellFormula(String from, String to) {
      super("SUM(" + from + ":" + to + ")");
    }
  }

  public static class CountRangeCellFormula extends CellFormula {

    public CountRangeCellFormula(String from, String to) {
      super("COUNTA(" + from + ":" + to + ")");
    }
  }

  public static class SumListCellFormula extends CellFormula {

    public SumListCellFormula(String colCharacter, List<Integer> rowIndexes) {
      super(createFunction(colCharacter, rowIndexes));
    }

    private static final String createFunction(String colCharacter, List<Integer> rowIndexes) {
      final StringBuilder sb = new StringBuilder();
      for (Integer rowIndex : rowIndexes) {
        if (sb.length() > 0) {
          sb.append("+");
        }
        sb.append(colCharacter + (1 + rowIndex));
      }
      return sb.toString();
    }
  }
}
