/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s): ___________
 ************************************************************************
 */

OB.OBUIREP = {};
OB.OBUIREP.manageViewButtonProperties = {
  disabled: false,
  buttonType: 'manageviews',
  prompt: OB.I18N.getLabel('OBUIAPP_ManageViews_Toolbar_Button'),
  sortPosition: 30,
  keyboardShortcutId: 'Reporting_SaveReportStructure',
  //			  keyboardShortcutId: 'ToolBar_ManageViews',
  initWidget: function () {
    this.menu = isc.Menu.create({
      button: this,

      // overridden to get much simpler custom style name
      getBaseStyle: function (record, rowNum, colNum) {
        if (colNum === 0) {
          return this.baseStyle + 'Icon';
        }
        if (record.showSeparator) {
          return this.baseStyle + 'Separator';
        }
        return this.baseStyle;
      },

      itemClick: function (item, colNum) {
        var view = this.button.view;
        if (item.personalizationId) {
          view.applyViewDefinition(item.viewDefinition);
        } else {
          item.doClick(this.button.view);
        }
      }
    }, OB.Styles.Personalization.Menu);
    this.Super('initWidget', arguments);
  },

  showMenu: function () {
    if (!OB.Utilities.checkProfessionalLicense(
    OB.I18N.getLabel('OBUIAPP_ActivateMessageWindowPersonalization'))) {
      return;
    }
    return this.Super('showMenu', arguments);
  },

  // shows the menu with the available views and the save 
  // and delete option
  action: function () {
    var data = [],
        icon, i, item, undef, view, reportTabView = this.view,
        adminLevel = reportTabView.isPersonalizationAdmin,
        viewDefinition, length, viewSelected = false,
        viewDefinitions = reportTabView.viewDefinitions ? reportTabView.viewDefinitions : [],
        canDelete = false;

    if (!OB.Utilities.checkProfessionalLicense(
    OB.I18N.getLabel('OBUIAPP_ActivateMessagePersonalization'))) {
      return;
    }

    // add the standard view, but make a copy so that it is not added
    // to the real list of editable/deletable views
    viewDefinitions = isc.shallowClone(viewDefinitions);
    if (reportTabView.originalViewDefinition) {
      viewDefinitions.push(reportTabView.originalViewDefinition);
    }

    // create the list of current views to show
    length = viewDefinitions.length;
    for (i = 0; i < length; i++) {
      viewDefinition = viewDefinitions[i];
      canDelete = viewDefinition.canEdit || canDelete;

      if (reportTabView.currentViewDefinition && reportTabView.currentViewDefinition.personalizationId === viewDefinition.personalizationId) {
        icon = this.menu.itemIcon;
      } else {
        icon = null;
      }

      item = {
        title: viewDefinition.name,
        icon: icon,
        personalizationId: viewDefinition.personalizationId,
        viewDefinition: viewDefinition
      };

      data.push(item);
    }

    // compute the menu items, only if the user is allowed
    // to personalize
    if (this.isWindowPersonalizationAllowed()) {

      data.push({
        title: OB.I18N.getLabel('OBUIAPP_SaveView'),
        showSeparator: data.length > 0,
        doClick: function (view) {
          var popup = isc.OBPopup.create({
            view: view
          }, OB.Personalization.ManageViewsPopupProperties, OB.OBUIREP.ManageViewsPopupPropertiesSave, adminLevel ? OB.Styles.Personalization.saveViewPopupLarge : OB.Styles.Personalization.saveViewPopupSmall);
          popup.show();
        }
      });

      // if there are views allow to choose a default
      if (viewDefinitions.length > 0) {
        data.push({
          title: OB.I18N.getLabel('OBUIAPP_SetDefaultView'),
          doClick: function (view) {
            var popup = isc.OBPopup.create({
              view: view
            }, OB.Personalization.ManageViewsPopupProperties, OB.OBUIREP.ManageViewsPopupPropertiesDefault, OB.Styles.Personalization.deleteViewPopup);
            popup.show();
          }
        });
      }

      // only show the delete option if there are deletable options        
      if (canDelete) {
        data.push({
          title: OB.I18N.getLabel('OBUIAPP_DeleteView'),
          doClick: function (view) {
            var popup = isc.OBPopup.create({
              view: view
            }, OB.Personalization.ManageViewsPopupProperties, OB.OBUIREP.ManageViewsPopupPropertiesDelete, OB.Styles.Personalization.deleteViewPopup);
            popup.show();
          }
        });
      }
    }

    if (data.length === 0) {
      // this can not really happen, the button should be disabled
      return;
    }

    this.menu.setData(data);

    this.Super('action', arguments);
  },

  updateState: function () {
    this.resetBaseStyle();

    // no items are shown in this case
    if (!this.isWindowPersonalizationAllowed() && !this.viewsToSelect()) {
      this.setDisabled(true);
    } else {
      this.setDisabled(false);
    }

    this.show();
  },

  viewsToSelect: function () {
    return this.view.viewDefinitions && this.view.viewDefinitions.length > 0;
  },

  isWindowPersonalizationAllowed: function () {
    var propValue, undef, view = this.view;

    // note: false is not cached as during initialization
    // things can be false
    if (this.userWindowPersonalizationAllowed === undef) {
      // if an admin then allow personalization
      if (view.isPersonalizationAdmin) {
        this.userWindowPersonalizationAllowed = true;
      } else {
        propValue = OB.PropertyStore.get('OBUIAPP_WindowPersonalization_Override', view._windowId ? view._windowId : null);
        if (propValue === 'false' || propValue === 'N') {
          return false;
        } else {
          this.userWindowPersonalizationAllowed = true;
        }
      }
    }
    return this.userWindowPersonalizationAllowed;
  }
};

OB.OBUIREP.ManageViewsPopupPropertiesDefault = isc.addProperties({}, OB.Personalization.ManageViewsPopupPropertiesDefault, {

  getFields: function () {
    var i, value, views = this.view.viewDefinitions ? this.view.viewDefinitions : [],
        valueMap = {},
        flds = [],
        length;

    if (views) {
      length = views.length;
      for (i = 0; i < length; i++) {
        valueMap[views[i].personalizationId] = views[i].name;
      }
      valueMap[this.view.originalViewDefinition.personalizationId] = this.view.originalViewDefinition.name;
    }

    flds[0] = isc.addProperties({
      name: 'personalization',
      title: OB.I18N.getLabel('OBUIAPP_DefaultView'),
      valueMap: valueMap,
      editorType: 'select',
      addUnknownValues: false,
      required: true,
      changed: function () {
        // enable the save button when there is a change
        this.form.saveButton.setDisabled(false);
        // don't let it be disabled again
        this.form.toggleSave = false;
      }
    }, OB.Styles.Personalization.viewFieldDefaults, OB.Styles.OBFormField.DefaultComboBox);

    // set the value
    value = this.view.defaultPersonalizationId;
    if (value && flds[0].valueMap[value]) {
      flds[0].value = value;
    } else {
      flds[0].value = 'dummyId';
    }

    return flds;
  },

  // do the set default action
  doAction: function (form) {
    var personalizationId = form.getValue("personalization");
    this.view.setDefaultViewDefinitionId(personalizationId);
  }
});

OB.OBUIREP.ManageViewsPopupPropertiesDelete = {
  title: OB.I18N.getLabel('OBUIAPP_DeleteView'),
  actionLabel: OB.I18N.getLabel('OBUIAPP_Delete'),

  // creates one combo with the viewdefinitions which can
  // be deleted by the current user
  getFields: function () {
    var i, value, views = this.view.viewDefinitions ? this.view.viewDefinitions : [],
        valueMap = {},
        flds = [],
        length;

    if (views) {
      length = views.length;
      for (i = 0; i < length; i++) {
        if (views[i].canEdit) {
          valueMap[views[i].personalizationId] = views[i].name;
        }
      }
    }

    flds[0] = isc.addProperties({
      name: 'personalization',
      title: OB.I18N.getLabel('OBUIAPP_View'),
      valueMap: valueMap,
      editorType: 'select',
      required: true,
      allowEmptyValue: true
    }, OB.Styles.Personalization.viewFieldDefaults, OB.Styles.OBFormField.DefaultComboBox);
    return flds;
  },

  // do the delete action
  doAction: function (form) {
    var personalizationId = form.getValue("personalization");
    this.view.deleteViewDefinition(personalizationId);
  }
};

OB.OBUIREP.ManageViewsPopupPropertiesSave = {
  title: OB.I18N.getLabel('OBUIAPP_SaveView'),

  actionLabel: OB.I18N.getLabel('OBUIAPP_Save'),

  // 3 combo fields are created: views, level and level value
  // the last 2 are only created if the user is allowed to
  // change or set views for different levels
  getFields: function () {
    var i, formData, valueMap = {},
        levelMapSet = false,
        levelMap = {
        '': ''
        },
        flds = [],
        length, views = this.view.viewDefinitions;

    // create the view combo
    if (views) {
      length = views.length;
      for (i = 0; i < length; i++) {
        if (views[i].canEdit) {
          valueMap[views[i].personalizationId] = views[i].name;
        }
      }
    }

    flds[0] = isc.addProperties({
      name: 'personalization',
      title: OB.I18N.getLabel('OBUIAPP_SaveAs'),
      valueMap: valueMap,
      editorType: 'ComboBoxItem',
      allowEmptyValue: true,
      required: true,

      // if changed, then set the level and levelvalue
      // fields to the current level of the personalization
      changed: function (form, item, value) {
        var i, levelField = form.getField('level'),
            length, levelValueField = form.getField('levelValue');

        // find the personalization
        if (levelField && views) {
          // and the view, and set the level and level value
          // combos
          length = views.length;
          for (i = 0; i < length; i++) {
            if (views[i].personalizationId === value) {
              if (views[i].clientId) {
                levelField.storeValue('clients');
                levelValueField.storeValue(views[i].clientId);
              }
              if (views[i].orgId) {
                levelField.storeValue('orgs');
                levelValueField.storeValue(views[i].orgId);
              }
              if (views[i].roleId) {
                levelField.storeValue('roles');
                levelValueField.storeValue(views[i].roleId);
              }
              form.setValue('default', views[i].isDefault);
              levelField.updateValueMap(true);
              levelValueField.updateValueMap(true);
            }
          }
        }
      }
    }, OB.Styles.Personalization.viewFieldDefaults, OB.Styles.OBFormField.DefaultComboBox);

    // create the level combo
    if (this.view.personalizationData) {
      formData = this.view.personalizationFormData;
      // note the key in the levelMap (clients, orgs, roles) corresponds
      // to the property name in the formData
      if (formData.clients) {
        levelMap.clients = OB.I18N.getLabel("OBUIAPP_Client");
        levelMapSet = true;
      }
      if (formData.orgs) {
        levelMap.orgs = OB.I18N.getLabel("OBUIAPP_Organization");
        levelMapSet = true;
      }
      if (formData.roles) {
        levelMap.roles = OB.I18N.getLabel("OBUIAPP_Role");
        levelMapSet = true;
      }
    }

    // if the user is allowed to set views on different 
    // levels, then create the 2 combos
    if (levelMapSet) {
      flds[1] = isc.addProperties({
        name: 'level',
        title: OB.I18N.getLabel('OBUIAPP_Level'),
        valueMap: levelMap,
        editorType: 'select',
        defaultToFirstOption: true,
        emptyDisplayValue: OB.I18N.getLabel('OBUIAPP_User'),
        changed: function (form, item, value) {
          // if the level combo changes, then set the
          // level value map (so that it shows clients, orgs
          // or roles resp.)
          var levelValueField = form.getField('levelValue');
          levelValueField.setValueMap(formData[value]);
          levelValueField.clearValue();
        }
      }, OB.Styles.Personalization.viewFieldDefaults, OB.Styles.OBFormField.DefaultComboBox);

      flds[2] = isc.addProperties({
        name: 'levelValue',
        title: OB.I18N.getLabel('OBUIAPP_Value'),
        valueMap: {},
        editorType: 'select',
        emptyDisplayValue: OB.User.userName,
        defaultToFirstOption: true
      }, OB.Styles.Personalization.viewFieldDefaults, OB.Styles.OBFormField.DefaultComboBox);

      // and create the checkbox to let it be the default 
      // for other users
      flds[3] = isc.addProperties({
        name: 'default',
        title: OB.I18N.getLabel('OBUIAPP_DefaultView'),
        editorType: 'OBCheckboxItem'
      }, OB.Styles.Personalization.viewFieldDefaults, OB.Styles.OBFormField.DefaultCheckbox);
    }
    return flds;
  },

  doAction: function (form) {
    var name, levelInformation = {},
        persId, level = form.getValue('level'),
        levelValue = form.getValue('levelValue');

    if (level === 'clients' && levelValue) {
      levelInformation.clientId = levelValue;
    }
    if (level === 'roles' && levelValue) {
      levelInformation.roleId = levelValue;
    }
    if (level === 'orgs' && levelValue) {
      levelInformation.orgId = levelValue;
    }
    if (!levelInformation.clientId && !levelInformation.orgId && !levelInformation.roleId) {
      levelInformation.userId = OB.User.id;
    }
    persId = form.getValue("personalization");
    name = form.getField("personalization").getDisplayValue();

    // same value, the user typed in a new name
    if (name === persId) {
      persId = null;
    }

    this.view.storeViewDefinition(levelInformation, persId, name, form.getValue('default'));
  }
};