/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

isc.defineClass("OBUIREP_ReportingProcessView", isc.OBTabSet).addProperties({
  width: '100%',
  height: '100%',
  tabs: [],

  initWidget: function () {
    var i, tab;

    for (i = 0; i < this.parameters.tabs.length; i++) {
      tab = this.parameters.tabs[i];
      if (tab.exportExcel) {
        this.showExcelButton = true;
      }
      this.tabs.push({
        title: tab.tabTitle,
        pane: isc.OBUIREP_TabView.create(tab)
      });
      // the tabs have the same process parameters
      // just take them
      this.processParameters = tab.processParameters;
    }
    this.Super('initWidget', arguments);
  },

  exportAsExcel: function () {
    var params = isc.clone(this.processParameters);
    params._action = this.processParameters.actionHandler;
    params.mode = 'VALIDATE';
    params.processId = this.processParameters.processId;
    params.view = this.parameters._processView;
    params.view.showProcessing(true);
    tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
    tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Executing', [params.view.tabTitle]));

    OB.RemoteCallManager.call(params._action, {}, params, function (resp, data, req) {
      if (data) {
        if ('true' === data.success) {
          params.mode = 'BUILD';
          params.type = 'XLS';
          OB.RemoteCallManager.call(params._action, {}, params, function (resp, data, req) {
            params.mode = 'DOWNLOAD';
            params.tmpfileName = data.tmpfileName;
            params.fileName = data.fileName;
            tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
            tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Done', [params.view.tabTitle]));
            params.view.showProcessing(false);
            //Copied from ob-parameter-window-view.js
            if (params.view.toolBarLayout) {
              for (i = 0; i < params.view.toolBarLayout.children.length; i++) {
                if (params.view.toolBarLayout.children[i].show) {
                  params.view.toolBarLayout.children[i].show();
                }
              }
            }
            if (params.view.popupButtons) {
              params.view.popupButtons.show();
            }
            OB.Utilities.postThroughHiddenForm(OB.Application.contextUrl + 'org.openbravo.client.kernel', params);
          });
        } else if (data.error) {
          isc.warn(OB.I18N.getLabel(data.error));
        } else if (data.warning) {
          params.mode = 'BUILD';
          params.type = 'XLS';
          isc.ask(OB.I18N.getLabel(data.warning), function (clickOK) {
            if (clickOK) {
              OB.RemoteCallManager.call(params._action, {}, params, function (resp, data, req) {
                params.mode = 'DOWNLOAD';
                params.tmpfileName = data.tmpfileName;
                params.fileName = data.fileName;
                tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
                tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Done', [params.view.tabTitle]));
                params.view.showProcessing(false);
                //Copied from ob-parameter-window-view.js
                if (params.view.toolBarLayout) {
                  for (i = 0; i < params.view.toolBarLayout.children.length; i++) {
                    if (params.view.toolBarLayout.children[i].show) {
                      params.view.toolBarLayout.children[i].show();
                    }
                  }
                }
                if (params.view.popupButtons) {
                  params.view.popupButtons.show();
                }
                OB.Utilities.postThroughHiddenForm(OB.Application.contextUrl + 'org.openbravo.client.kernel', params);
              });
            }
          });

        }
      }
    });
  }
}, OB.Styles.Personalization.TabSet);

isc.defineClass("OBUIREP_TabView", isc.VLayout).addProperties({
  width: '100%',
  height: '100%',
  viewDefinitions: [],

  SAVE_BUTTON_PROPERTIES: {
    action: function () {
      var view = this.view,
          func = function (ok) {
          if (ok) {
            view.storeViewDefinition();
          }
          };
      isc.confirm(OB.I18N.getLabel('OBUIREP_ConfirmSaveStructure'), func);
    },
    disabled: false,
    buttonType: 'saveView',
    // 'manageviews',
    // buttonType: 'save',
    sortPosition: 30,
    prompt: OB.I18N.getLabel('OBUIREP_SaveReportStructure'),
    updateState: function () {},
    keyboardShortcutId: 'Reporting_SaveReportStructure'
  },

  EXPORT_CSV_BUTTON_PROPERTIES: {
    action: function () {
      this.view.exportAsCSV();
    },
    disabled: false,
    buttonType: isc.OBToolbar.TYPE_EXPORT,
    sortPosition: 40,
    prompt: OB.I18N.getLabel('OBUIAPP_ExportGrid'),
    updateState: function () {},
    keyboardShortcutId: 'ToolBar_Export'
  },

  EDITHILITE_BUTTON_PROPERTIES: {
    action: function () {
      this.view.grid.editHilites();
    },
    disabled: false,
    buttonType: 'analytics',
    sortPosition: 20,
    prompt: OB.I18N.getLabel('OBUIREP_EditHilites'),
    updateState: function () {},
    keyboardShortcutId: 'Reporting_EditHilite'
  },

  initWidget: function () {
    var i, view = this,
        formData;

    this.Super('initWidget', arguments);

    this.toolbar = isc.OBToolbar.create({
      view: this,
      width: '100%',
      leftMembers: [
      isc.OBToolbarIconButton.create(
      isc.OBToolbar.REFRESH_BUTTON_PROPERTIES, {
        sortPosition: 10,
        action: function () {
          view.grid.invalidateCache();
        }
      }), isc.OBToolbarIconButton.create(
      OB.OBUIREP.manageViewButtonProperties, {
        view: this
      }), isc.OBToolbarIconButton.create(
      this.EXPORT_CSV_BUTTON_PROPERTIES, {
        view: this
      })],
      // ,
      // isc.OBToolbarIconButton.create(this.EDITHILITE_BUTTON_PROPERTIES)],
      rightMembers: []
    });

    this.addMember(this.toolbar);

    this.personalizationFormData = this.personalizationData.formData;
    formData = this.personalizationFormData;

    this.viewDefinitions = [];
    for (i = 0; i < this.personalizationData.views.length; i++) {
      if (this.personalizationData.views[i].viewDefinition.tabId === this._tabId) {
        // repair one indirection
        isc.addProperties(this.personalizationData.views[i], this.personalizationData.views[i].viewDefinition);
        this.viewDefinitions.push(this.personalizationData.views[i]);
      }
    }
    this.isPersonalizationAdmin = (formData.clients || formData.orgs || formData.roles);

    // sort the viewdefinitions
    this.viewDefinitions.sort(function (v1, v2) {
      var t1 = v1.name,
          t2 = v2.name;
      if (t1 < t2) {
        return -1;
      } else if (t1 === t2) {
        return 0;
      }
      return 1;
    });

    // get the datasource, when it is received
    // build the grid
    OB.Datasource.get(this.dataSourceId, this, null, true);
  },

  exportAsCSV: function () {
    var params = isc.clone(this.processParameters),
        validateParams = isc.clone(this.processParameters);
    params._action = this.processParameters.actionHandler;
    params.mode = 'CSV';
    params.processId = this.processParameters.processId;
    params.fields = isc.shallowClone(this.grid.getFields());
    params.data = isc.shallowClone(this.grid.getOriginalData().localData);

    // first validate
    validateParams._action = this.processParameters.actionHandler;
    validateParams.mode = 'VALIDATE';

    // note if the data is too large, chrome will change to multi-part
    // post
    // this results in that parameters are not parsed on the server, the
    // action param is then null.
    if (params.data && params.data.length > 1000) {
      isc.warn(OB.I18N.getLabel('OBCRQAN_ExportCSVLargeDataSet'));
      delete params.data;
    }
    params.tabId = this._tabId;
    params.windowId = this._windowId;
    params._UTCOffsetMiliseconds = OB.Utilities.Date.getUTCOffsetInMiliseconds();

    OB.RemoteCallManager.call(validateParams._action, {}, validateParams, function (
    resp, data, req) {
      if (data) {
        if ('true' === data.success) {
          OB.Utilities.postThroughHiddenForm(OB.Application.contextUrl + 'org.openbravo.client.kernel', params);
        } else if (data.error) {
          isc.warn(OB.I18N.getLabel(data.error));
        } else if (data.warning) {
          isc.ask(OB.I18N.getLabel(data.warning), function (clickOK) {
            if (clickOK) {
              OB.Utilities.postThroughHiddenForm(OB.Application.contextUrl + 'org.openbravo.client.kernel', params);
            }
          });
        }
      }
    });
  },

  setDataSource: function (ds) {
    var msgObject, msg;

    ds.requestProperties = ds.requestProperties || {};
    ds.requestProperties.params = ds.requestProperties.params || {};
    isc.addProperties(ds.requestProperties.params, this.processParameters);

    ds.handleError = function (response, request) {
      if (response && response.httpResponseText) {
        msgObject = isc.JSON.decode(response.httpResponseText);
        if (msgObject && msgObject.response && msgObject.response.error && msgObject.response.error.message) {
          OB.KernelUtilities.handleUserException(msgObject.response.error.message);
        }
      }
    }

    ds.dropUnknownCriteria = false;

    this.grid = isc.OBUIREP_ReportingProcessGrid.create({
      dataSource: ds,
      fields: this.fields
    });

    this.addMember(this.grid);

    this.initializeViewDefinitions();
  },

  initializeViewDefinitions: function () {
    var personalizationPreference, personalizationPreferenceByTab, i, viewDefinition;

    this.originalViewDefinition = {
      personalizationId: 'dummyId',
      originalView: true,
      name: OB.I18N.getLabel('OBUIAPP_StandardView'),
      canEdit: false,
      viewDefinition: this.grid.getViewState(true)
    };

    // we use a class property to keep track of the preference accross
    // several
    // tabs
    // as several tabs maybe open at the same time
    OB.OBUIREP.reportPersonalizations = OB.OBUIREP.reportPersonalizations || {};
    if (!OB.OBUIREP.reportPersonalizations[this._windowId]) {
      OB.OBUIREP.reportPersonalizations[this._windowId] = OB.PropertyStore.get('OBUIAPP_DefaultSavedView', this._windowId);
    }
    personalizationPreference = OB.OBUIREP.reportPersonalizations[this._windowId];
    if (personalizationPreference) {
      personalizationPreferenceByTab = personalizationPreference;
      if (personalizationPreferenceByTab && personalizationPreferenceByTab[this._tabId]) {
        this.defaultPersonalizationId = personalizationPreferenceByTab[this._tabId];
        for (i = 0; i < this.viewDefinitions.length; i++) {
          if (this.viewDefinitions[i].personalizationId === this.defaultPersonalizationId) {
            viewDefinition = this.viewDefinitions[i];
            break;
          }
        }
      }
    }

    if (viewDefinition) {
      this.applyViewDefinition(viewDefinition);
    }
  },

  setDefaultViewDefinitionId: function (personalizationId) {
    var personalizationPreference, personalizationPreferenceByTab;

    OB.OBUIREP.reportPersonalizations = OB.OBUIREP.reportPersonalizations || {};
    if (!OB.OBUIREP.reportPersonalizations[this._windowId]) {
      OB.OBUIREP.reportPersonalizations[this._windowId] = OB.PropertyStore.get('OBUIAPP_DefaultSavedView', this._windowId);
    }
    personalizationPreference = OB.OBUIREP.reportPersonalizations[this._windowId];
    if (personalizationPreference) {
      personalizationPreferenceByTab = personalizationPreference;
    } else {
      personalizationPreferenceByTab = {};
    }
    this.defaultPersonalizationId = personalizationId;
    personalizationPreferenceByTab[this._tabId] = personalizationId;
    OB.OBUIREP.reportPersonalizations[this._windowId] = personalizationPreferenceByTab;
    OB.PropertyStore.set('OBUIAPP_DefaultSavedView', OB.OBUIREP.reportPersonalizations[this._windowId], this._windowId);
  },

  _storeViewDefinition: function () {
    var params = {
      action: 'store',
      target: 'viewDefinition',
      clientId: this._clientId,
      orgId: this._orgId,
      roleId: this._roleId,
      userId: this._userId,
      windowId: this._windowId,
      tabId: this._tabId,
      applyLevelInformation: true
    },
        callback = function () {},
        viewDefinition = this.grid.getViewDefinition(true);

    // and store on the server
    OB.RemoteCallManager.call('org.openbravo.client.application.personalization.PersonalizationActionHandler', viewDefinition, params, callback);
  },

  deleteViewDefinition: function (personalizationId) {
    var me = this,
        viewDefinitions = this.viewDefinitions;
    OB.RemoteCallManager.call('org.openbravo.client.application.personalization.PersonalizationActionHandler', {}, {
      personalizationId: personalizationId,
      action: 'delete'
    }, function (resp, data, req) {
      var length, i, views = me.viewDefinitions ? me.viewDefinitions : [];

      if (viewDefinitions) {
        length = viewDefinitions.length;
        // remove the entry from the global list
        for (i = 0; i < length; i++) {
          if (viewDefinitions[i].personalizationId === personalizationId) {
            viewDefinitions.splice(i, 1);
            break;
          }
        }
        if (me.defaultPersonalizationId === personalizationId) {
          me.setDefaultViewDefinitionId(null);
        }
      }
    });
  },

  applyViewDefinition: function (viewDefinition) {
    this.currentViewDefinition = viewDefinition;
    this.grid.setViewState(viewDefinition.viewDefinition);
  },

  storeViewDefinition: function (levelInformation, persId, name, isDefault) {
    var params, personalizationData = {},
        me = this;
    personalizationData.name = name;
    personalizationData.tabId = this._tabId;
    personalizationData.viewDefinition = this.grid.getViewState(true);
    if (persId) {
      personalizationData.personalizationId = persId;
    }

    // if there is a personalization id then use that
    // this ensures that a specific record will be updated
    // on the server.
    // the target means that only the view property of the total
    // user interface personalization is stored.
    // also persist the level information
    if (persId) {
      params = {
        action: 'store',
        target: 'viewDefinition',
        clientId: levelInformation.clientId,
        orgId: levelInformation.orgId,
        roleId: levelInformation.roleId,
        userId: levelInformation.userId,
        windowId: this._windowId,
        tabId: this._tabId,
        personalizationId: persId,
        applyLevelInformation: true
      };

    } else {
      // this case is used if there is no personalization record
      // use the level information to store the view state
      params = {
        action: 'store',
        target: 'viewDefinition',
        clientId: levelInformation.clientId,
        orgId: levelInformation.orgId,
        roleId: levelInformation.roleId,
        userId: levelInformation.userId,
        windowId: this._windowId,
        tabId: this._tabId,
        applyLevelInformation: true
      };
    }

    // and store on the server
    OB.RemoteCallManager.call('org.openbravo.client.application.personalization.PersonalizationActionHandler', personalizationData, params, function (resp, data, req) {
      var i = 0,
          fnd = false,
          length, newViewDefinition, viewDefinitions = me.viewDefinitions;

      // create a new structure, the same way as it is
      // returned from the server
      newViewDefinition = isc.addProperties({
        canEdit: true,
        personalizationId: data.personalizationId
      }, levelInformation, personalizationData);

      // when returning update the in-memory entry,
      // first check if there is an existing record, if so
      // update it
      if (viewDefinitions) {
        length = viewDefinitions.length;
        for (i = 0; i < length; i++) {
          if (viewDefinitions[i].personalizationId === data.personalizationId) {
            viewDefinitions[i] = newViewDefinition;
            fnd = true;
            break;
          }
        }
      }

      // not found create a new one, take into account
      if (!fnd) {
        viewDefinitions.push(newViewDefinition);
        if (isDefault) {
          me.setDefaultViewDefinitionId(data.personalizationId);
        }

        // sort the viewdefinitions
        viewDefinitions.sort(function (v1, v2) {
          var t1 = v1.name,
              t2 = v2.name;
          if (t1 < t2) {
            return -1;
          } else if (t1 === t2) {
            return 0;
          }
          return 1;
        });
      }
    });
  }
});