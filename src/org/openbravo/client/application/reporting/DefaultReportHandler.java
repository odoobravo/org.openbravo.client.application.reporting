/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2014 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.client.application.reporting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.domaintype.DateDomainType;
import org.openbravo.base.model.domaintype.DomainType;
import org.openbravo.base.model.domaintype.EnumerateDomainType;
import org.openbravo.base.model.domaintype.TimestampDomainType;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.util.OBClassLoader;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.Process;
import org.openbravo.client.application.personalization.PersonalizationHandler;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.window.OBViewFieldHandler.OBViewField;
import org.openbravo.client.application.window.OBViewTab;
import org.openbravo.client.application.window.ReportFieldUtils;
import org.openbravo.client.kernel.KernelConstants;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.client.kernel.reference.NumberUIDefinition;
import org.openbravo.client.kernel.reference.UIDefinition;
import org.openbravo.client.kernel.reference.UIDefinitionController;
import org.openbravo.dal.core.DalContextListener;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.SessionInfo;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.security.UsageAudit;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.erpCommon.utility.PropertyNotFoundException;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.service.datasource.DataSourceService;
import org.openbravo.service.datasource.DataSourceServiceProvider;
import org.openbravo.service.datasource.DefaultDataSourceService;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.json.DefaultJsonDataService.QueryResultWriter;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.service.json.JsonUtils;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.Replace;

/**
 * Creates the input for showing the data in a report.
 * 
 * @author mtaal
 */
public class DefaultReportHandler extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(DefaultReportHandler.class);

  // helps reporting datasources to throw errors/exceptions when more is loaded
  // note the client will ask for 10001 therefore the limit is 10002
  // the client can then show a message if more is returned
  public static final int LOAD_LIMIT = 10002;

  private static final int MAX_BLOCKED_USERS = 5;

  private static final Map<String, String> executingUsers = new ConcurrentHashMap<String, String>();
  private static final Map<String, String> validatingUsers = new ConcurrentHashMap<String, String>();

  private static void doSynchronizedPdfExport(DefaultReportHandler reportHandler,
      Map<String, String> requestParameters, JSONObject data) {
    if (isOverUsed()) {
      // ignore the request, can't give a message back
      return;
    }
    // release anything we did before as we can get into wait status afterwards
    OBDal.getInstance().commitAndClose();
    doActualSynchronizedPdfExport(reportHandler, requestParameters, data);
  }

  private static synchronized void doActualSynchronizedPdfExport(
      DefaultReportHandler reportHandler, Map<String, String> requestParameters, JSONObject data) {
    final String userId = getUser();
    if (userId != null) {
      executingUsers.put(userId, userId);
    }
    try {
      if (data == null) {
        reportHandler.pdfExport(requestParameters);
      } else {
        reportHandler.pdfExport(data, requestParameters);
      }
    } finally {
      if (userId != null) {
        executingUsers.remove(userId);
      }
    }
  }

  private static JSONObject doSynchronizedPreValidate(DefaultReportHandler reportHandler,
      Map<String, Object> parameters, String content) {
    if (isOverUsed()) {
      return getOverUsageJSON();
    }
    // release anything we did before as we can get into wait status afterwards
    OBDal.getInstance().commitAndClose();
    return doActualSynchronizedPreValidate(reportHandler, parameters, content);
  }

  private static synchronized JSONObject doActualSynchronizedPreValidate(
      DefaultReportHandler reportHandler, Map<String, Object> parameters, String content) {
    final String userId = getUser();
    if (userId != null) {
      validatingUsers.put(userId, userId);
    }
    try {
      return reportHandler.preValidate(parameters, content);
    } finally {
      if (userId != null) {
        validatingUsers.remove(userId);
      }
    }
  }

  private static boolean isOverUsed() {
    if (executingUsers.size() >= MAX_BLOCKED_USERS) {
      return true;
    }
    if (validatingUsers.size() >= MAX_BLOCKED_USERS) {
      return true;
    }
    final String userId = getUser();
    if (userId == null) {
      return false;
    }
    if (validatingUsers.containsKey(userId) || executingUsers.containsKey(userId)) {
      return true;
    }
    return false;
  }

  private static String getUser() {
    return (String) RequestContext.get().getSession().getAttribute("#Authenticated_user");
  }

  private static JSONObject getOverUsageJSON() {
    try {
      final JSONObject result = new JSONObject();
      result.put("error", "OBUIREP_TooManyUsersExecutingReport");
      return result;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  @Inject
  private DataSourceServiceProvider dataSourceServiceProvider;

  @Inject
  private PersonalizationHandler personalizationHandler;

  @Inject
  private WeldUtils weldUtils;

  public void execute() {
    final HttpServletRequest request = RequestContext.get().getRequest();
    long time = System.currentTimeMillis();
    String mode = request.getParameter("mode");
    if (mode == null) {
      mode = "Default";
    }
    boolean err = true;

    try {
      if ("CSV".equals(request.getParameter("mode"))) {
        try {
          final JSONArray jsonData = request.getParameter("data") == null ? null : new JSONArray(
              request.getParameter("data"));
          final JSONArray jsonFields = new JSONArray(request.getParameter("fields"));

          final Map<String, String> parameterMap = new HashMap<String, String>();
          for (Enumeration<?> keys = request.getParameterNames(); keys.hasMoreElements();) {
            final String key = (String) keys.nextElement();
            if (request.getParameterValues(key) != null
                && request.getParameterValues(key).length > 1) {
              parameterMap.put(key, request.getParameter(key));
            } else {
              parameterMap.put(key, request.getParameter(key));
            }
          }
          csvExport(parameterMap, jsonFields, jsonData);
          err = false;
          return;
        } catch (Exception e) {
          throw new IllegalStateException(e);
        }
      } else if ("XLS".equals(request.getParameter("mode"))) {
        log.error("Use parameter BUILD instead of XLS");
        try {
          final Map<String, String> parameterMap = new HashMap<String, String>();
          for (Enumeration<?> keys = request.getParameterNames(); keys.hasMoreElements();) {
            final String key = (String) keys.nextElement();
            if (request.getParameterValues(key) != null
                && request.getParameterValues(key).length > 1) {
              parameterMap.put(key, request.getParameter(key));
            } else {
              parameterMap.put(key, request.getParameter(key));
            }
          }
          xlsExport(parameterMap);
          err = false;
          return;
        } catch (Exception e) {
          throw new IllegalStateException(e);
        }
      } else if ("PDF".equals(request.getParameter("mode"))) {
        log.error("Use parameter BUILD instead of PDF");
        final Map<String, String> parameterMap = new HashMap<String, String>();
        for (Enumeration<?> keys = request.getParameterNames(); keys.hasMoreElements();) {
          final String key = (String) keys.nextElement();
          if (request.getParameterValues(key) != null && request.getParameterValues(key).length > 1) {
            parameterMap.put(key, request.getParameter(key));
          } else {
            parameterMap.put(key, request.getParameter(key));
          }
        }
        doSynchronizedPdfExport(this, parameterMap, null);
        err = false;
        return;
      } else if ("VALIDATE".equals(request.getParameter("mode"))) {
        try {
          final BufferedReader reader = new BufferedReader(new InputStreamReader(
              request.getInputStream(), "UTF-8"));
          String line;
          StringBuilder sb = new StringBuilder();
          while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
          }
          final String content = (sb.length() > 0 ? sb.toString() : null);

          final Map<String, Object> parameterMap = new HashMap<String, Object>();
          for (Enumeration<?> keys = request.getParameterNames(); keys.hasMoreElements();) {
            final String key = (String) keys.nextElement();
            if (request.getParameterValues(key) != null
                && request.getParameterValues(key).length > 1) {
              parameterMap.put(key, request.getParameterValues(key));
            } else {
              parameterMap.put(key, request.getParameter(key));
            }
          }
          // also add the Http Stuff
          parameterMap.put(KernelConstants.HTTP_SESSION, request.getSession(false));
          parameterMap.put(KernelConstants.HTTP_REQUEST, request);

          final HttpServletResponse response = RequestContext.get().getResponse();
          response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
          response.setHeader("Content-Type", JsonConstants.JSON_CONTENT_TYPE);
          response.getWriter().write(
              doSynchronizedPreValidate(this, parameterMap, content).toString());
          response.getWriter().close();
          err = false;
          return;
        } catch (IOException e) {
          throw new OBException(e);
        }
      } else if ("BUILD".equals(request.getParameter("mode"))) {
        try {

          final Map<String, String> parameterMap = new HashMap<String, String>();
          for (Enumeration<?> keys = request.getParameterNames(); keys.hasMoreElements();) {
            final String key = (String) keys.nextElement();
            if (request.getParameterValues(key) != null
                && request.getParameterValues(key).length > 1) {
              parameterMap.put(key, request.getParameter(key));
            } else {
              parameterMap.put(key, request.getParameter(key));
            }
          }

          JSONObject data = new JSONObject();

          if ("PDF".equals(request.getParameter("type"))) {
            doSynchronizedPdfExport(this, parameterMap, data);
          } else if ("XLS".equals(request.getParameter("type"))) {
            xlsExport(data, parameterMap);
          }

          parameterMap.put(KernelConstants.HTTP_SESSION, request.getSession(false).toString());
          parameterMap.put(KernelConstants.HTTP_REQUEST, request.toString());

          final HttpServletResponse response = RequestContext.get().getResponse();
          response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
          response.setHeader("Content-Type", JsonConstants.JSON_CONTENT_TYPE);
          PrintWriter out = response.getWriter();
          out.print(data);

          response.getWriter().close();

          err = false;
          return;
        } catch (IOException e) {
          throw new OBException(e);
        }
      } else if ("DOWNLOAD".equals(request.getParameter("mode"))) {
        final Map<String, String> parameterMap = new HashMap<String, String>();
        for (Enumeration<?> keys = request.getParameterNames(); keys.hasMoreElements();) {
          final String key = (String) keys.nextElement();
          if (request.getParameterValues(key) != null && request.getParameterValues(key).length > 1) {
            parameterMap.put(key, request.getParameter(key));
          } else {
            parameterMap.put(key, request.getParameter(key));
          }
        }
        try {
          doDownload(request, parameterMap);
          err = false;
          return;
        } catch (IOException e) {
          err = true;
          log.error("Error downloading the file: " + e.getMessage(), e);
        }
        return;
      }
      super.execute();
      err = false;
    } finally {
      if (!err) {
        // set the command so it is being used by the audit action no dal
        SessionInfo.setCommand(mode);
        SessionInfo.setProcessType("R");
        SessionInfo.setModuleId(getModuleId());
        String processId = request.getParameter("processId");
        if (processId == null) {
          processId = "Default";
        }
        SessionInfo.setProcessId(processId);
        UsageAudit.auditActionNoDal(new DalConnectionProvider(), new VariablesSecureApp(request),
            this.getClass().getName(), (System.currentTimeMillis() - time));
      }
    }
  }

  protected String getModuleId() {
    return "2C88FFC67CE7417F9B051ABE8DDA3C41";
  }

  /**
   * Downloads the file with the report result. The file is stored in a temporary folder with a
   * generated name. It is renamed and download as an attachment of the response. Once it is
   * finished the file is removed from the server.
   */
  private void doDownload(HttpServletRequest request, Map<String, String> parameters)
      throws IOException {
    final String strFileName = (String) parameters.get("fileName");
    final String tmpFileName = (String) parameters.get("tmpfileName");
    String expType = null;

    if (StringUtils.endsWithIgnoreCase(strFileName, "pdf")) {
      expType = "PDF";
    } else if (StringUtils.endsWithIgnoreCase(strFileName, "xlsx")) {
      expType = "XLS";
    } else {
      throw new IllegalArgumentException("Trying to download report file with unsupported type "
          + strFileName);
    }

    final String tmpDirectory = System.getProperty("java.io.tmpdir");
    final File file = new File(tmpDirectory, tmpFileName);
    FileUtility fileUtil = new FileUtility(tmpDirectory, tmpFileName, false, true);
    try {
      final HttpServletResponse response = RequestContext.get().getResponse();

      response.setHeader("Content-Type", "application/" + expType);
      response.setContentType("application/" + expType);
      response.setCharacterEncoding("UTF-8");
      // TODO: Compatibility code with IE8. To be reviewed when its support is stopped.
      // see issue #29109
      String userAgent = request.getHeader("user-agent");
      if (userAgent.contains("MSIE")) {
        response.setHeader("Content-Disposition",
            "attachment; filename=\"" + URLEncoder.encode(strFileName, "utf-8") + "\"");
      } else {
        response.setHeader("Content-Disposition",
            "attachment; filename=\"" + MimeUtility.encodeWord(strFileName, "utf-8", "Q") + "\"");
      }
      fileUtil.dumpFile(response.getOutputStream());
      response.getOutputStream().flush();
      response.getOutputStream().close();
    } finally {
      if (file.exists()) {
        file.delete();
      }
    }
  }

  protected JSONObject preValidate(Map<String, Object> parameters, String content) {
    try {
      final JSONObject result = new JSONObject();
      result.put("success", "true");
      return result;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();
    try {
      final Map<String, Object> allParams = new HashMap<String, Object>();
      allParams.putAll(parameters);
      JSONObject params = new JSONObject(content).getJSONObject("_params");
      final Iterator<?> iterator = params.keys();
      while (iterator.hasNext()) {
        final Object key = iterator.next();
        allParams.put((String) key, params.get((String) key));
      }

      final JSONObject recordInfo = new JSONObject();
      final JSONObject error = validateReturnErrors(allParams);
      if (error != null) {
        recordInfo.put("error", error);
      } else {
        recordInfo.put("tabs", getTabDefinitions(allParams, content));
      }
      params.put("processId", parameters.get("processId"));
      params.put("actionHandler", this.getClass().getName());
      recordInfo.put("processParameters", params);

      final JSONObject reportAction = new JSONObject();
      reportAction.put(getOpenActionName(), recordInfo);

      final JSONArray actions = new JSONArray();
      actions.put(0, reportAction);
      result.put("responseActions", actions);

      result.put("retryExecution", true);
      result.put("showResultsInProcessView", true);

      return result;
    } catch (Exception e) {
      log.error("Error in process", e);
      return new JSONObject();
    }
  }

  protected JSONObject validateReturnErrors(Map<String, Object> parameters) throws JSONException {
    return null;
  }

  protected String getOpenActionName() {
    return "OBUIREP_openProcessReport";
  }

  protected JSONArray getTabDefinitions(Map<String, Object> parameters, String content)
      throws JSONException {
    JSONObject params = new JSONObject(content).getJSONObject("_params");

    final JSONArray tabDefinitions = new JSONArray();

    final Process process = OBDal.getInstance().get(Process.class, parameters.get("processId"));
    params.put("processId", process.getId());
    params.put("actionHandler", this.getClass().getName());

    int i = 0;
    for (ProcessReportTab tab : process.getOBUIREPProcessReportTabList()) {
      final JSONObject tabJson = getTabDefinition(tab, parameters);
      tabJson.put("processParameters", params);

      tabJson.put("personalizationData",
          personalizationHandler.getPersonalizationForWindow(tab.getTab().getWindow()));
      tabDefinitions.put(i++, tabJson);
    }

    return tabDefinitions;
  }

  protected JSONObject getTabDefinition(ProcessReportTab reportTab, Map<String, Object> parameters)
      throws JSONException {
    final JSONObject tabDefinition = new JSONObject();

    final OBViewTab viewTab = WeldUtils.getInstanceFromStaticBeanManager(OBViewTab.class);
    viewTab.setTab(reportTab.getTab());
    final JSONArray fieldsJson = new JSONArray();
    int i = 0;
    final List<OBViewField> flds = new ArrayList<OBViewField>(ReportFieldUtils.getFields(viewTab));
    Collections.sort(flds, new FieldComparator());

    for (OBViewField field : flds) {
      if (!field.getIsGridProperty()) {
        continue;
      }

      final JSONObject fieldJson = new JSONObject();
      fieldJson.put("name", field.getName());
      fieldJson.put("title", field.getLabel());
      fieldJson.put("clientClass", field.getClientClass());
      fieldJson.put("targetEntity", field.getTargetEntity());

      if (!field.isDisplayed() || !field.isShowInitiallyInGrid()) {
        fieldJson.put("hidden", true);
      }

      if (field.isShowSummary()) {
        fieldJson.put("showSummary", field.isShowSummary());
        fieldJson.put("showGroupSummary", field.isShowSummary());
        if (!"".equals(field.getSummaryFunction())) {
          fieldJson.put("summaryFunction", field.getSummaryFunction());
        } else {
          fieldJson.put("summaryFunction", "sum");
        }
      }
      fieldJson.put("type", field.getType());

      fieldJson.put("sort", field.getGridSort());
      if (field.getAutoExpand()) {
        fieldJson.put("autoExpand", field.getAutoExpand());
      }
      if (field.getCellAlign() != null) {
        fieldJson.put("cellAlign", field.getCellAlign());
      }
      if (!field.isShowInitiallyInGrid()) {
        fieldJson.put("showIf", false);
      }
      // add field/grid and other props
      String subProperties = field.getFieldProperties() + field.getFilterEditorProperties();
      if (subProperties.trim().startsWith(",")) {
        subProperties = "'_dummy' : '_dummy'" + subProperties;
      }
      subProperties = subProperties.replace(",,", ",");
      final JSONObject props = new JSONObject("{" + subProperties + "}");
      final Iterator<?> ks = props.keys();
      while (ks.hasNext()) {
        final Object k = ks.next();
        fieldJson.put((String) k, props.get((String) k));
      }
      updateFieldJson(reportTab, fieldJson);
      fieldsJson.put(i++, fieldJson);
    }

    final String dsId;
    if (ApplicationConstants.TABLEBASEDTABLE.equals(reportTab.getTab().getTable()
        .getDataOriginType())) {
      dsId = reportTab.getTab().getTable().getName();
    } else {
      dsId = reportTab.getTab().getTable().getObserdsDatasource().getId();
    }

    tabDefinition.put("dataSourceId", dsId);
    tabDefinition.put("tabTitle", viewTab.getTabTitle());
    if (reportTab.getProcessDefintion().getObuirepXltemplate() != null) {
      tabDefinition.put("exportExcel", true);
    }
    tabDefinition.put("_clientId", OBContext.getOBContext().getCurrentClient().getId());
    tabDefinition.put("_orgId", OBContext.getOBContext().getCurrentOrganization().getId());
    tabDefinition.put("_roleId", OBContext.getOBContext().getRole().getId());
    tabDefinition.put("_userId", OBContext.getOBContext().getUser().getId());
    tabDefinition.put("_tabId", reportTab.getTab().getId());
    tabDefinition.put("_windowId", DalUtil.getId(reportTab.getTab().getWindow()));
    tabDefinition.put("fields", fieldsJson);

    return tabDefinition;
  }

  protected void updateFieldJson(ProcessReportTab reportTab, JSONObject fieldJson)
      throws JSONException {
  }

  public void csvExport(Map<String, String> requestParameters, JSONArray jsonFields,
      JSONArray jsonData) {

    OBContext.setAdminMode(true);
    try {
      final Process process = OBDal.getInstance().get(Process.class,
          requestParameters.get("processId"));
      final String tabId = requestParameters.get("tabId");
      final HttpServletResponse response = RequestContext.get().getResponse();
      response.setHeader("Content-Type", "text/csv");
      response.setContentType("text/csv");
      response.setHeader("Content-Disposition",
          "attachment; filename=\"" + getCSVFileName(process, requestParameters) + "\"");
      final JSONToCSVWriter jsonCsvWriter = new JSONToCSVWriter(requestParameters, jsonFields,
          response.getWriter());
      jsonCsvWriter.writeHeader();
      if (jsonData != null) {
        for (int i = 0; i < jsonData.length(); i++) {
          jsonCsvWriter.write(jsonData.getJSONObject(i));
        }
      } else {
        boolean csvExportPossible = false;
        for (ProcessReportTab processTab : process.getOBUIREPProcessReportTabList()) {
          if (tabId.equals(DalUtil.getId(processTab.getTab()))) {
            final DataSourceService dsService = getDataSource(processTab.getTab());
            if (dsService instanceof DefaultDataSourceService) {
              csvExportPossible = true;
              ((DefaultDataSourceService) dsService).fetch(requestParameters, jsonCsvWriter);
            }
          }
        }
        if (!csvExportPossible) {
          throw new OBException("The datasource does not support CSV export in a flush-mode method");
        }
      }

    } catch (Exception e) {
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public void xlsExport(Map<String, String> requestParameters) {
    OBContext.setAdminMode(true);
    try {
      final Process process = OBDal.getInstance().get(Process.class,
          requestParameters.get("processId"));
      final XSSFWorkbook workbook;
      if (process.getObuirepXltemplate() == null) {
        workbook = new XSSFWorkbook();
      } else {
        final String xlsPath = process.getObuirepXltemplate();
        String realPath = DalContextListener.getServletContext().getRealPath(xlsPath);
        File xlsFile = new File(realPath);
        if (xlsFile.exists()) {
          final FileInputStream fis = new FileInputStream(xlsFile);
          workbook = new XSSFWorkbook(fis);
          fis.close();
        } else {
          final InputStream is = this.getClass().getResourceAsStream(xlsPath);
          if (is == null) {
            workbook = new XSSFWorkbook();
          } else {
            workbook = new XSSFWorkbook(is);
          }
        }
      }
      for (ProcessReportTab processTab : process.getOBUIREPProcessReportTabList()) {
        final DataSourceService dsService = getDataSource(processTab.getTab());
        if (dsService instanceof ExporterToXLS) {
          final ExporterToXLS exporter = (ExporterToXLS) dsService;
          exporter.exportToXLS(processTab, requestParameters, workbook);
        }
      }

      evaluateAll(workbook);

      final HttpServletResponse response = RequestContext.get().getResponse();
      response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
      response.setHeader("Content-Type",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      response.setHeader("Content-Disposition",
          "attachment; filename=\"" + getXLSFileName(process, workbook, requestParameters) + "\"");

      workbook.write(response.getOutputStream());

      response.getOutputStream().close();

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public void xlsExport(JSONObject data, Map<String, String> requestParameters) {
    OBContext.setAdminMode(true);
    try {
      final Process process = OBDal.getInstance().get(Process.class,
          requestParameters.get("processId"));
      final XSSFWorkbook workbook;
      if (process.getObuirepXltemplate() == null) {
        workbook = new XSSFWorkbook();
      } else {
        final String xlsPath = process.getObuirepXltemplate();
        String realPath = DalContextListener.getServletContext().getRealPath(xlsPath);
        File xlsFile = new File(realPath);
        if (xlsFile.exists()) {
          final FileInputStream fis = new FileInputStream(xlsFile);
          workbook = new XSSFWorkbook(fis);
          fis.close();
        } else {
          final InputStream is = this.getClass().getResourceAsStream(xlsPath);
          if (is == null) {
            workbook = new XSSFWorkbook();
          } else {
            workbook = new XSSFWorkbook(is);
          }
        }
      }
      for (ProcessReportTab processTab : process.getOBUIREPProcessReportTabList()) {
        final DataSourceService dsService = getDataSource(processTab.getTab());
        if (dsService instanceof ExporterToXLS) {
          final ExporterToXLS exporter = (ExporterToXLS) dsService;
          exporter.exportToXLS(processTab, requestParameters, workbook);
        }
      }

      evaluateAll(workbook);
      String strFileName = getXLSFileName(process, workbook, requestParameters);
      String strTmpFileName = UUID.randomUUID().toString() + "." + "XLS";

      File tmpFile = new File(System.getProperty("java.io.tmpdir"), strTmpFileName);
      OutputStream rptTmpOS = new FileOutputStream(tmpFile);
      workbook.write(rptTmpOS);
      data.put("fileName", strFileName);
      data.put("tmpfileName", strTmpFileName);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  protected void evaluateAll(XSSFWorkbook workbook) {
    final XSSFFormulaEvaluator evaluator = new XSSFFormulaEvaluator(workbook);
    evaluator.clearAllCachedResultValues();

    for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
      final Sheet sheet = workbook.getSheetAt(i);

      for (Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();) {
        final Row r = rit.next();

        for (@SuppressWarnings("rawtypes")
        Iterator cit = r.cellIterator(); cit.hasNext();) {
          XSSFCell c = (XSSFCell) cit.next();
          if (c.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
            // evaluator.evaluateFormulaCell(c);
            // evaluator.evaluateInCell(c);
          }
        }
      }
    }
    evaluator.clearAllCachedResultValues();
  }

  public void pdfExport(Map<String, String> requestParameters) {
    OBContext.setAdminMode(true);
    try {
      final Process process = OBDal.getInstance().get(Process.class,
          requestParameters.get("processId"));

      final HttpServletResponse response = RequestContext.get().getResponse();
      response.setHeader("Content-Type", "application/pdf");
      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition",
          "attachment; filename=\"" + getPDFFileName(process, requestParameters) + "\"");

      exportToPDF(process, new HashMap<String, Object>(requestParameters),
          response.getOutputStream());

      response.getOutputStream().close();

    } catch (Exception e) {
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public void pdfExport(JSONObject data, Map<String, String> requestParameters) {
    OBContext.setAdminMode(true);
    try {
      final Process process = OBDal.getInstance().get(Process.class,
          requestParameters.get("processId"));

      String expType = "PDF";
      if (StringUtils.isNotEmpty(process.getObuirepXltemplate())
          && StringUtils.isEmpty(getJasperReportFilePath())) {
        expType = "XLS";
      }

      String strFileName = getPDFFileName(process, requestParameters);
      String strTmpFileName = UUID.randomUUID().toString() + "." + expType;

      File tmpFile = new File(System.getProperty("java.io.tmpdir"), strTmpFileName);

      OutputStream rptTmpOS = new FileOutputStream(tmpFile);

      exportToPDF(process, new HashMap<String, Object>(requestParameters), rptTmpOS);
      rptTmpOS.close();

      data.put("tmpfileName", strTmpFileName);
      data.put("fileName", strFileName);

    } catch (Exception e) {
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public String getXLSFileName(Process process, XSSFWorkbook workBook,
      Map<String, String> parameters) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat(OBPropertiesProvider.getInstance()
        .getOpenbravoProperties().getProperty("dateTimeFormat.java"));
    return getSafeFilename(process.getName() + "-" + dateFormat.format(new Date())) + ".xlsx";
  }

  public String getPDFFileName(Process process, Map<String, String> parameters) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat(OBPropertiesProvider.getInstance()
        .getOpenbravoProperties().getProperty("dateTimeFormat.java"));
    return getSafeFilename(process.getName() + "-" + dateFormat.format(new Date())) + ".pdf";
  }

  public String getCSVFileName(Process process, Map<String, String> parameters) {
    final SimpleDateFormat dateFormat = new SimpleDateFormat(OBPropertiesProvider.getInstance()
        .getOpenbravoProperties().getProperty("dateTimeFormat.java"));
    return getSafeFilename(process.getName() + "-" + dateFormat.format(new Date())) + ".csv";
  }

  private DataSourceService getDataSource(Tab tab) throws ClassNotFoundException {
    if (!ApplicationConstants.DATASOURCEBASEDTABLE.equals(tab.getTable().getDataOriginType())) {
      return dataSourceServiceProvider.getDataSource(tab.getTable().getName());
    }

    @SuppressWarnings("unchecked")
    final Class<DataSourceService> clz = (Class<DataSourceService>) OBClassLoader.getInstance()
        .loadClass(tab.getTable().getObserdsDatasource().getJavaClassName());
    final DataSourceService dsService = weldUtils.getInstance(clz);
    dsService.setName(tab.getTable().getName());
    dsService.setEntity(ModelProvider.getInstance().getEntityByTableId(tab.getTable().getId()));
    return dsService;
  }

  public static String getSafeFilename(String name) {
    return name.replaceAll("[:\\\\/*?|<>]", "_");
  }

  private static class FieldComparator implements Comparator<OBViewField> {

    @Override
    public int compare(OBViewField o1, OBViewField o2) {
      return o1.getGridSort() - o2.getGridSort();
    }
  }

  public void exportToPDF(Process process, Map<String, Object> parameters, OutputStream os) {
    final String filePath = DalContextListener.getServletContext().getRealPath(
        getJasperReportFilePath());
    final int lastSegmentIndex = filePath.lastIndexOf("/");
    final String fileDir;
    if (lastSegmentIndex != -1) {
      fileDir = filePath.substring(0, lastSegmentIndex + 1);
    } else {
      fileDir = "";
    }
    parameters.put("SUBREPORT_DIR", fileDir);
    parameters.put("OBCONTEXT", OBContext.getOBContext());

    ReportingUtils.exportToPDF(process, parameters, os, filePath);
  }

  protected String getJasperReportFilePath() {
    return "";
  }

  public static class JSONToCSVWriter extends QueryResultWriter {

    private String fieldSeparator;
    private String decimalSeparator;
    private String prefDecimalSeparator;
    private SimpleDateFormat jsonDateFormat = JsonUtils.createDateFormat();
    private SimpleDateFormat jsonDateTimeFormat = JsonUtils.createDateTimeFormat();
    private SimpleDateFormat reportDateFormat;
    private SimpleDateFormat reportDateTimeFormat;
    private Map<String, Map<String, String>> refLists = new HashMap<String, Map<String, String>>();
    private List<String> refListCols = new ArrayList<String>();
    private List<String> fields = new ArrayList<String>();
    private List<String> fieldTitles = new ArrayList<String>();
    private Map<String, DecimalFormat> formats = new HashMap<String, DecimalFormat>();
    private Map<String, UIDefinition> uiDefinitions = new HashMap<String, UIDefinition>();
    private Map<String, UIDefinition> uiDefinitionsByField = new HashMap<String, UIDefinition>();
    private int clientUTCOffsetMiliseconds;
    private TimeZone clientTimeZone;
    private Writer writer;
    private int rows = 0;

    public JSONToCSVWriter(Map<String, String> parameters, JSONArray jsonFields, Writer writer) {
      this.writer = writer;

      final VariablesSecureApp vars = new VariablesSecureApp(RequestContext.get().getRequest());
      {
        final String pattern = RequestContext.get().getSessionAttribute("#AD_JAVADATETIMEFORMAT")
            .toString();
        reportDateTimeFormat = new SimpleDateFormat(pattern);
        reportDateTimeFormat.setLenient(true);
      }
      {
        final String pattern = RequestContext.get().getSessionAttribute("#AD_JAVADATEFORMAT")
            .toString();
        reportDateFormat = new SimpleDateFormat(pattern);
        reportDateFormat.setLenient(true);
      }

      // cache the ui defs
      for (UIDefinition uiDefinition : UIDefinitionController.getInstance().getAllUIDefinitions()) {
        uiDefinitions.put(uiDefinition.getName(), uiDefinition);
      }

      try {
        OBContext.setAdminMode();
        final Tab tab = OBDal.getInstance().get(Tab.class, parameters.get("tabId"));
        try {
          prefDecimalSeparator = Preferences.getPreferenceValue("OBSERDS_CSVDecimalSeparator",
              true, OBContext.getOBContext().getCurrentClient(), OBContext.getOBContext()
                  .getCurrentOrganization(), OBContext.getOBContext().getUser(), OBContext
                  .getOBContext().getRole(), tab.getWindow());
        } catch (PropertyNotFoundException ignore) {
          // There is no preference for the decimal separator.
        }

        decimalSeparator = UIDefinitionController.getInstance()
            .getFormatDefinition("generalQty", "Edition").getDecimalSymbol();
        try {
          fieldSeparator = Preferences.getPreferenceValue("OBSERDS_CSVFieldSeparator", true,
              OBContext.getOBContext().getCurrentClient(), OBContext.getOBContext()
                  .getCurrentOrganization(), OBContext.getOBContext().getUser(), OBContext
                  .getOBContext().getRole(), tab.getWindow());
        } catch (PropertyNotFoundException ignore) {
          // There is no preference for the field separator. Using the default one.
          fieldSeparator = ",";
        }
        if ((prefDecimalSeparator != null && prefDecimalSeparator.equals(fieldSeparator))
            || (prefDecimalSeparator == null && decimalSeparator.equals(fieldSeparator))) {
          if (!fieldSeparator.equals(";")) {
            fieldSeparator = ";";
          } else {
            fieldSeparator = ",";
          }
          log.warn("Warning: CSV Field separator is identical to the decimal separator. Changing the field separator to "
              + fieldSeparator + " to avoid generating a wrong CSV file");
        }
        if (parameters.get("_UTCOffsetMiliseconds").length() > 0) {
          clientUTCOffsetMiliseconds = Integer.parseInt(parameters.get("_UTCOffsetMiliseconds"));
        } else {
          clientUTCOffsetMiliseconds = 0;
        }

        final String userLanguageId = OBContext.getOBContext().getLanguage().getId();

        clientTimeZone = null;
        try {
          String clientTimeZoneId = Preferences.getPreferenceValue("localTimeZoneID", true,
              OBContext.getOBContext().getCurrentClient(), OBContext.getOBContext()
                  .getCurrentOrganization(), OBContext.getOBContext().getUser(), OBContext
                  .getOBContext().getRole(), null);
          List<String> validTimeZoneIDs = Arrays.asList(TimeZone.getAvailableIDs());
          if (validTimeZoneIDs.contains(clientTimeZoneId)) {
            clientTimeZone = TimeZone.getTimeZone(clientTimeZoneId);
          } else {
            log.error(clientTimeZoneId
                + " is not a valid time zone identifier. For a list of all accepted identifiers check http://www.java2s.com/Tutorial/Java/0120__Development/GettingallthetimezonesIDs.htm");
          }
        } catch (PropertyException pe) {
          log.warn("The local Local Timezone ID property is not defined. It can be defined in a preference. For a list of all accepted values check http://www.java2s.com/Tutorial/Java/0120__Development/GettingallthetimezonesIDs.htm");
        }
        for (int i = 0; i < jsonFields.length(); i++) {
          final JSONObject field = jsonFields.getJSONObject(i);
          // ignore those...
          if (!field.has("type")) {
            continue;
          }
          final String type = field.getString("type");
          if (!uiDefinitions.containsKey(type)) {
            continue;
          }
          final UIDefinition uiDefinition = uiDefinitions.get(type);
          final String name = field.getString("name");
          uiDefinitionsByField.put(name, uiDefinition);
          fieldTitles.add(field.getString("title"));
          if (uiDefinition instanceof NumberUIDefinition) {
            formats.put(name,
                Utility.getFormat(vars, ((NumberUIDefinition) uiDefinition).getFormat()));
          }
          fields.add(name);

          if (!(uiDefinition.getDomainType() instanceof EnumerateDomainType)) {
            continue;
          }
          String referenceId = uiDefinition.getReference().getId();
          Map<String, String> reflists = new HashMap<String, String>();
          final String hql = "select al.searchKey, al.name from ADList al where "
              + " al.reference.id=? and al.active=true";
          final Query qry = OBDal.getInstance().getSession().createQuery(hql);
          qry.setString(0, referenceId);
          for (Object o : qry.list()) {
            final Object[] row = (Object[]) o;
            reflists.put(row[0].toString(), row[1].toString());
          }
          final String hqltrl = "select al.searchKey, trl.name from ADList al, ADListTrl trl where "
              + " al.reference.id=? and trl.listReference=al and trl.language.id=?"
              + " and al.active=true and trl.active=true";
          final Query qrytrl = OBDal.getInstance().getSession().createQuery(hqltrl);
          qrytrl.setString(0, referenceId);
          qrytrl.setString(1, userLanguageId);
          for (Object o : qrytrl.list()) {
            final Object[] row = (Object[]) o;
            reflists.put(row[0].toString(), row[1].toString());
          }
          refListCols.add(name);
          refLists.put(name, reflists);
        }
      } catch (Exception e) {
        throw new OBException("Error while exporting a CSV file", e);
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    public void writeHeader() {
      try {
        OBContext.setAdminMode();
        if (fieldTitles.size() > 0) {
          // If the request came with the view state information, we get the properties from there
          for (int i = 0; i < fieldTitles.size(); i++) {
            if (i > 0) {
              writer.append(fieldSeparator);
            }
            if (fieldTitles.get(i) != null) {
              writer.append("\"").append(fieldTitles.get(i)).append("\"");
            }
          }
        }
      } catch (Exception e) {
        throw new OBException("Error while exporting a CSV file", e);
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    public void write(JSONObject row) {
      try {
        OBContext.setAdminMode();
        boolean isFirst = true;
        writer.append("\n");
        for (String fieldName : fields) {
          try {
            if (isFirst) {
              isFirst = false;
            } else {
              writer.append(fieldSeparator);
            }
            if (!row.has(fieldName)) {
              continue;
            }
            final Object value = row.get(fieldName);
            if (value == JSONObject.NULL || value == null || "".equals(value)
                || "null".equals(value)) {
              continue;
            }
            final DomainType domainType = uiDefinitionsByField.get(fieldName).getDomainType();

            Object reportValue = value;
            if (refListCols.contains(fieldName)) {
              reportValue = refLists.get(fieldName).get(value);
            } else if (value instanceof Number) {
              DecimalFormat format = formats.get(fieldName);
              if (format == null) {
                reportValue = value.toString().replace(".", decimalSeparator);
              } else {
                reportValue = format.format(new BigDecimal(value.toString()));
                if (prefDecimalSeparator != null) {
                  reportValue = reportValue
                      .toString()
                      .replace(
                          new Character(format.getDecimalFormatSymbols().getDecimalSeparator())
                              .toString(),
                          prefDecimalSeparator);
                }

              }
            } else if (domainType instanceof TimestampDomainType) {
              final String repairedString = JsonUtils.convertFromXSDToJavaFormat(value.toString());
              final Date localDate = jsonDateTimeFormat.parse(repairedString);
              final Date clientTimezoneDate = convertFromLocalToClientTimezone(localDate);
              reportValue = reportDateTimeFormat.format(clientTimezoneDate);
            } else if (domainType instanceof DateDomainType) {
              final Date date = jsonDateFormat.parse(value.toString());
              reportValue = reportDateFormat.format(date);
            }

            if (reportValue != null) {
              reportValue = Replace.replace(reportValue.toString(), "\"", "\"\"");
            } else {
              reportValue = "";
            }
            if (!(value instanceof Number)) {
              reportValue = "\"" + reportValue + "\"";
            }
            writer.append(reportValue.toString());
          } catch (Exception e) {
            throw new OBException("Error while exporting CSV information field " + fieldName
                + " of " + row, e);
          }
        }

        // flush to the client every 1000 rows
        rows++;
        if ((rows % 1000) == 0) {
          writer.flush();
        }
      } catch (Exception e) {
        throw new OBException("Error while exporting a CSV file", e);
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    private Date convertFromLocalToClientTimezone(Date localDate) {

      Date UTCDate = convertFromLocalToUTCTimezone(localDate);
      Date clientDate = convertFromUTCToClientTimezone(UTCDate);

      return clientDate;
    }

    private Date convertFromUTCToClientTimezone(Date UTCdate) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(UTCdate);
      if (clientTimeZone != null) {
        calendar = Calendar.getInstance(clientTimeZone);
        calendar.setTime(UTCdate);
        int gmtMillisecondOffset = (calendar.get(Calendar.ZONE_OFFSET) + calendar
            .get(Calendar.DST_OFFSET));
        calendar.add(Calendar.MILLISECOND, gmtMillisecondOffset);
      } else {
        calendar = Calendar.getInstance();
        calendar.setTime(UTCdate);
        calendar.add(Calendar.MILLISECOND, clientUTCOffsetMiliseconds);
      }
      return calendar.getTime();
    }

    private Date convertFromLocalToUTCTimezone(Date localDate) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(localDate);

      int gmtMillisecondOffset = (calendar.get(Calendar.ZONE_OFFSET) + calendar
          .get(Calendar.DST_OFFSET));
      calendar.add(Calendar.MILLISECOND, -gmtMillisecondOffset);

      return calendar.getTime();
    }
  }

}
