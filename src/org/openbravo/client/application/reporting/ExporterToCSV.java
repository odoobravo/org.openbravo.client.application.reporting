/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.client.application.reporting;

import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.openbravo.client.application.reporting.DefaultReportHandler.JSONToCSVWriter;

/**
 * Implementors support exporting to xls.
 * 
 * @author mtaal
 */
public interface ExporterToCSV {

  /**
   * The exporter should use the
   * {@link JSONToCSVWriter#writeRow(java.io.Writer, org.codehaus.jettison.json.JSONObject)} method
   * to write individual rows. For informational purposes the field definition is passed in JSON.
   */
  public void exportToCSV(ProcessReportTab reportTab, Map<String, String> parameters,
      JSONArray jsonFields, JSONToCSVWriter csvWriter);
}
