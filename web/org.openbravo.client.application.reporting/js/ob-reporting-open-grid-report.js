/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */


OB.Utilities.Action.set('OBUIREP_openProcessReport', function (paramObj) {
  var i, processView = paramObj._processView,
      mainLayout = processView.resultLayout,
      reportView, exportXLSButton, saveAsJobButton, btns = [];

  if (paramObj.error) {
    // an error get rid of the buttons!
    processView.toolBarLayout.removeAllLeftMembers();
    delete processView.toolBarLayout.reportButtonsAdded;
    isc.warn(paramObj.error.message);
    return;
  }

  reportView = isc.OBUIREP_ReportingProcessView.create({
    parameters: paramObj
  });
  mainLayout.addMember(reportView);
  processView.currentReportView = reportView;

  // add buttons to the process view toolbar
  if (!processView.toolBarLayout.reportButtonsAdded) {
    processView.toolBarLayout.reportButtonsAdded = true;

    saveAsJobButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OBUIREP_SaveAsJob'),
      realTitle: '',
      click: function () {
        //        isc.say(reportView);
      }
    });
    //    btns.push(saveAsJobButton);
    if (reportView.showExcelButton) {
      exportXLSButton = isc.OBFormButton.create({
        title: OB.I18N.getLabel('OBUIREP_ExportXLS'),
        realTitle: '',
        click: function () {
          processView.currentReportView.exportAsExcel();
        }
      });
      btns.push(exportXLSButton);
    }
    processView.toolBarLayout.addLeftMembers(btns, 0);
  }
});