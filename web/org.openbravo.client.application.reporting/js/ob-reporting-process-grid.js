/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

isc.defineClass("OBUIREP_ReportingProcessGrid", isc.OBGrid).addProperties({

  // supported in mater smartclient version
  showCollapsedGroupSummary: true,
  canEditHilites: true,

  // supported
  showGroupSummary: true,
  showGroupSummaryInHeader: true,
  groupByMaxRecords: 100000,
  autoFitFieldWidths: true,
  autoFitFieldsFillViewport: true,
  autoFitWidthApproach: 'title',
  hiliteCanReplaceValue: true,
  canAddSummaryFields: true,
  canAddFormulaFields: true,
  canEdit: false,
  autoFetchData: true,
  canDragSelectText: true,
  canDragSelect: true,
  canResizeFields: true,
  canSelectCells: true,
  canMultiGroup: true,
  canMultiSort: true,
  dataFetchMode: 'local',
  showFilterEditor: true,
  allowFilterExpressions: true,
  showClippedValuesOnHover: true,
  canHover: false,
  height: '*',
  width: '100%',
  currencyFormat: 'amountInform',
  ungroupText: OB.I18N.getLabel('OBUIAPP_ungroup'),
  groupByText: OB.I18N.getLabel('OBUIAPP_GroupBy'),

  initWidget: function () {
    this.fields = this.prepareGridFields(this.fields);

    if (!this.amountFormat) {
      this.amountFormat = OB.Format.formats[this.currencyFormat || 'amountInform'] || '#,##0.00';
      // repair grouping
      this.amountFormat = this.amountFormat.replace('.', ';;;');
      this.amountFormat = this.amountFormat.replace(',', OB.Format.defaultGroupingSymbol);
      this.amountFormat = this.amountFormat.replace(';;;', OB.Format.defaultDecimalSymbol);
    }

    // re-use getCellValue to handle count and related functions
    this.summaryRowProperties = {
      showRecordComponents: false,
      cellHoverHTML: this.cellHoverHTML,

      getCellAlign: function (record, rowNum, colNum) {
        var fld = this.getFields()[colNum],
            isRTL = this.isRTL(),
            func = this.getGridSummaryFunction(fld),
            isSummary = record && (record[this.groupSummaryRecordProperty] || record[this.gridSummaryRecordProperty]);

        // the count of a character column should also be right aligned
        if (isSummary && func === 'count') {
          return isRTL ? isc.Canvas.LEFT : isc.Canvas.RIGHT;
        }

        return this.Super('getCellAlign', arguments);
      },

      getCellValue: function (record, recordNum, fieldNum, gridBody) {
        var field = this.getField(fieldNum),
            gridField, func = this.parentElement.getGridSummaryFunction(field),
            value = record && field ? (field.displayField ? record[field.displayField] : record[field.name]) : null;

        // get the summary function from the main grid
        if (!func) {
          delete field.summaryFunction;
        } else {
          field.summaryFunction = func;
        }

        // handle count much simpler than smartclient does
        // so no extra titles or formatting
        if (record && func === 'count' && value >= 0) {
          return value;
        }

        return this.Super('getCellValue', arguments);
      }
    };

    this.Super('initWidget', arguments);
  },

  dataArrived: function (startRow, endRow) {
    if (endRow > 10000) {
      isc.warn(OB.I18N.getLabel('OBCRQAN_LargeDataSetWarning'));
    }
  },

  // overridden to get simplified count display
  getCellValue: function (record, recordNum, fieldNum, gridBody) {
    var field = this.fields[fieldNum],
        func, isGroupOrSummary = record && (record[this.groupSummaryRecordProperty] || record[this.gridSummaryRecordProperty]);

    if (!field) {
      return this.Super('getCellValue', arguments);
    }

    if (isGroupOrSummary) {
      func = this.getGridSummaryFunction(field);
      // handle count much simpler than smartclient does
      // so no extra titles or formatting
      if (!this.getGroupByFields().contains(field.name) && func === 'count' && (record[field.name] === 0 || record[field.name])) {
        return record[field.name];
      }
    }
    return this.Super('getCellValue', arguments);
  },

  // prevent a jscript error if there are no group summary functions
  getGroupSummaryData: function () {
    var ret = this.Super('getGroupSummaryData', arguments);
    if (isc.isAn.Array(ret) && !ret[0]) {
      return [{}];
    }
    return ret;
  },

  // Overrides the standard SC function as that function
  // also returns the default summary function from the 
  // type definition. We only want the explicitly set
  // summary functions.
  getGridSummaryFunction: function (field) {
    if (!field) {
      return;
    }
    return field.summaryFunction;
  },

  // when the summary information changes, refresh
  // the grid in the correct way
  setSummaryFunctionActions: function (clear) {
    var i, noSummaryFunction;
    if (this.isGrouped) {
      this.regroup();
    }
    if (!clear) {
      if (!this.showGridSummary) {
        this.setShowGridSummary(true);
      }
      this.recalculateGridSummary();
    } else if (this.showGridSummary) {
      noSummaryFunction = true;
      for (i = 0; i < this.getFields().length; i++) {
        if (this.getFields()[i].summaryFunction) {
          noSummaryFunction = false;
          break;
        }
      }
      if (noSummaryFunction) {
        this.setShowGridSummary(false);
      } else {
        this.recalculateGridSummary();
      }
    }
  },

  getHeaderContextMenuItems: function (colNum) {
    var field = this.getField(colNum),
        i, summarySubMenu = [],
        grid = this,
        groupByFields = this.getGroupByFields(),
        type, isDate, isNumber, menuItems = this.Super('getHeaderContextMenuItems', arguments);


    // remove the group by menu option if the field is grouped 
    // and it does not have a submenu
    if (groupByFields && groupByFields.contains(field.name)) {
      for (i = 0; i < menuItems.length; i++) {
        if (menuItems[i].groupItem && !menuItems[i].submenu) {
          menuItems.removeAt(i);
          break;
        }
      }
    }

    if (field) {
      type = isc.SimpleType.getType(field.type);
      isDate = isc.SimpleType.inheritsFrom(type, 'date');
      isNumber = isc.SimpleType.inheritsFrom(type, 'integer') || isc.SimpleType.inheritsFrom(type, 'float');

      if (isNumber && !field.clientClass) {
        summarySubMenu.add({
          title: OB.I18N.getLabel('OBUIAPP_SummaryFunctionSum'),
          // enabled: field.summaryFunction != 'sum',
          checked: field.summaryFunction === 'sum',
          click: function (target, item) {
            field.summaryFunction = 'sum';
            grid.setSummaryFunctionActions();
          }
        });

        summarySubMenu.add({
          title: OB.I18N.getLabel('OBUIAPP_SummaryFunctionAvg'),
          // enabled: field.summaryFunction != 'avg',
          checked: field.summaryFunction === 'avg',
          click: function (target, item) {
            field.summaryFunction = 'avg';
            grid.setSummaryFunctionActions();
          }
        });
      }

      if (!field.clientClass) {
        summarySubMenu.add({
          title: OB.I18N.getLabel('OBUIAPP_SummaryFunctionMin'),
          checked: field.summaryFunction === 'min',
          click: function (target, item) {
            field.summaryFunction = 'min';
            grid.setSummaryFunctionActions();
          }
        });

        summarySubMenu.add({
          title: OB.I18N.getLabel('OBUIAPP_SummaryFunctionMax'),
          checked: field.summaryFunction === 'max',
          click: function (target, item) {
            field.summaryFunction = 'max';
            grid.setSummaryFunctionActions();
          }
        });
      }

      summarySubMenu.add({
        title: OB.I18N.getLabel('OBUIAPP_SummaryFunctionCount'),
        // enabled: field.summaryFunction != 'count',
        checked: field.summaryFunction === 'count',
        click: function (target, item) {
          field.summaryFunction = 'count';
          grid.setSummaryFunctionActions();
        }
      });

      menuItems.add({
        isSeparator: true
      });

      menuItems.add({
        groupItem: true,
        title: OB.I18N.getLabel('OBUIAPP_SetSummaryFunction'),
        fieldName: field.name,
        targetField: field,
        prompt: OB.I18N.getLabel('OBUIAPP_SetSummaryFunction_Description'),
        canSelectParent: true,
        submenu: summarySubMenu
      });

      if (field.summaryFunction) {
        menuItems.add({
          title: OB.I18N.getLabel('OBUIAPP_ClearSummaryFunction'),
          targetField: field,
          click: function (target, item) {
            delete field.summaryFunction;
            grid.setSummaryFunctionActions(true);
          }
        });
      }

      menuItems.add({
        title: OB.I18N.getLabel('OBUIAPP_ClearSummaries'),
        targetField: field,
        click: function (target, item) {
          var i, fld;
          for (i = 0; i < grid.getFields().length; i++) {
            fld = grid.getFields()[i];
            delete fld.summaryFunction;
          }
          grid.setSummaryFunctionActions(true);
        }
      });
    }

    // add the summary functions
    return menuItems;
  },

  getCellAlign: function (record, rowNum, colNum) {
    var fld = this.getFields()[colNum],
        isRTL = this.isRTL(),
        func = this.getGridSummaryFunction(fld),
        isSummary = record && (record[this.groupSummaryRecordProperty] || record[this.gridSummaryRecordProperty]);
    if (!fld.clientClass && rowNum === this.getEditRow()) {
      return 'center';
    }

    if (isSummary && func === 'count') {
      return isRTL ? isc.Canvas.LEFT : isc.Canvas.RIGHT;
    }

    return this.Super('getCellAlign', arguments);
  },


  // overridden to also store the group mode
  // http://forums.smartclient.com/showthread.php?p=93877#post93877
  getGroupState: function () {
    var i, fld, state = this.Super('getGroupState', arguments),
        result = {};
    result.groupByFields = state;
    result.groupingModes = {};
    for (i = 0; i < this.getFields().length; i++) {
      fld = this.getFields()[i];
      if (fld.groupingMode) {
        result.groupingModes[fld.name] = fld.groupingMode;
      }
    }
    return result;
  },

  setGroupState: function (state) {
    var i, fld, key;
    if (state && (state.groupByFields || state.groupByFields === '')) {
      if (state.groupingModes) {
        for (key in state.groupingModes) {
          if (state.groupingModes.hasOwnProperty(key)) {
            fld = this.getField(key);
            if (fld) {
              fld.groupingMode = state.groupingModes[key];
            }
          }
        }
      }
      this.Super('setGroupState', [state.groupByFields]);
    } else {
      // older state definition
      this.Super('setGroupState', arguments);
    }
  },

  // also store the summary functions
  getViewState: function (returnObject) {
    var i, fld, state = this.Super('getViewState', [returnObject || true]);

    // set summary information, can not be stored in the field state
    // because smartclient does not provide a nice override point
    // when setting the fieldstate back to also set the summary function
    state.summaryFunctions = {};
    for (i = 0; i < this.getAllFields().length; i++) {
      fld = this.getAllFields()[i];
      if (fld.summaryFunction && isc.isA.String(fld.summaryFunction)) {
        state.summaryFunctions[fld.name] = fld.summaryFunction;
      }
    }

    // get rid of the selected state
    delete state.selected;

    if (returnObject) {
      return state;
    }
    return '(' + isc.Comm.serialize(state, false) + ')';
  },

  formatCellValue: function (value, record, rowNum, colNum, grid) {
    var cur, formattedValue, field = this.getField(colNum);

    // a currency, show standard 2 digits
    if (isc.isA.Number(value) && field.currencyRecordProperty) {
      formattedValue = OB.Utilities.Number.JSToOBMasked(value, this.amountFormat, OB.Format.defaultDecimalSymbol, OB.Format.defaultGroupingSymbol, OB.Format.defaultGroupingSize);
      if (formattedValue === value) {
        formattedValue = this.applyCellTypeFormatters(value, record, field, rowNum, colNum);
      }
    } else {
      formattedValue = this.applyCellTypeFormatters(value, record, field, rowNum, colNum);
    }

    if (field.currencyRecordProperty && record[field.currencyRecordProperty] && value) {
      cur = record[field.currencyRecordProperty];
      if (cur.startsWith('_')) {
        return formattedValue + cur.replace('_', ' ');
      }
      return record[field.currencyRecordProperty] + ' ' + formattedValue;
    }
    if (value && !formattedValue) {
      return value;
    }
    return formattedValue;
  },

  setViewState: function (state, settingDefault) {
    var localState, i, fld, hasSummaryFunction;

    localState = this.evalViewState(state, 'viewState');

    // strange case, sometimes need to call twice
    if (isc.isA.String(localState)) {
      localState = this.evalViewState(state, 'viewState');
    }

    if (!localState) {
      return;
    }
    delete localState.selected;

    this.Super('setViewState', ['(' + isc.Comm.serialize(localState, false) + ')']);

    if (localState.summaryFunctions) {
      hasSummaryFunction = false;
      for (i = 0; i < this.getAllFields().length; i++) {
        fld = this.getAllFields()[i];
        if (localState.summaryFunctions[fld.name]) {
          hasSummaryFunction = true;
          fld.summaryFunction = localState.summaryFunctions[fld.name];
        } else {
          delete fld.summaryFunction;
        }
      }
      this.setShowGridSummary(hasSummaryFunction);
    }
  },

  prepareGridFields: function (fields) {
    var result = [],
        i, length = fields.length,
        stdProperties, fld, type, expandFieldNames, hoverFunction, yesNoFormatFunction;

    yesNoFormatFunction = function (value, record, rowNum, colNum, grid) {
      return OB.Utilities.getYesNoDisplayValue(value);
    };

    stdProperties = {
      canEditFormula: true,
      canEditSummary: true,
      canFilter: true,
      canFreeze: true,
      canGroupBy: true,
      canHide: true,
      canHilite: true,
      canReorder: true
    };

    for (i = 0; i < length; i++) {
      fld = fields[i];

      if (fld.showIf === false) {
        fld.hidden = true;
      }

      delete fld.showIf;

      // show clipped values,, don't hover
      fld.showHover = false;

      isc.addProperties(fld, stdProperties);

      if (fld.displaylength) {
        fld.width = isc.OBGrid.getDefaultColumnWidth(fld.displaylength);
      } else {
        fld.width = isc.OBGrid.getDefaultColumnWidth(30);
      }

      // if a client class/canvas field
      if (fld.clientClass) {
        if (fld.showGridSummary !== true && fld.showGridSummary !== false) {
          fld.showGridSummary = false;
        }
        if (fld.showGroupSummary !== true && fld.showGridSummary !== false) {
          fld.showGridSummary = false;
        }
      }

      // correct some stuff coming from the form fields
      if (fld.displayed === false) {
        fld.visible = true;
        fld.alwaysTakeSpace = true;
      }

      fld.canExport = (fld.canExport === false ? false : true);
      fld.canHide = (fld.canHide === false ? false : true);
      fld.canFilter = (fld.canFilter === false ? false : true);
      fld.filterOnKeypress = (fld.filterOnKeypress === false ? false : true);
      fld.escapeHTML = (fld.escapeHTML === false ? false : true);
      fld.prompt = fld.title;
      fld.disabled = false;

      if (fld.yesNo) {
        fld.formatCellValue = yesNoFormatFunction;
      }

      type = isc.SimpleType.getType(fld.type);

      if (type.filterEditorType && !fld.filterEditorType) {
        fld.filterEditorType = type.filterEditorType;
      }

      if (type.sortNormalizer) {
        fld.sortNormalizer = type.sortNormalizer;
      }

      if (!fld.filterEditorProperties) {
        fld.filterEditorProperties = {};
      }

      if (fld.fkField) {
        fld.displayField = fld.name + OB.Constants.FIELDSEPARATOR + OB.Constants.IDENTIFIER;
        fld.valueField = fld.name;
        fld.filterOnKeypress = false;
        fld.filterEditorProperties.displayField = OB.Constants.IDENTIFIER;
        fld.filterEditorProperties.valueField = OB.Constants.IDENTIFIER;
      }

      fld.filterEditorProperties.required = false;

      // get rid of illegal summary functions
      if (fld.summaryFunction && !isc.OBViewGrid.SUPPORTED_SUMMARY_FUNCTIONS.contains(fld.summaryFunction)) {
        delete fld.summaryFunction;
      }

      // add grouping stuff
      if (type.inheritsFrom === 'float' || type.inheritsFrom === 'integer') {
        // this is needed because of a bug in smartclient in Listgrid
        // only groupingmodes on type level are considered
        // http://forums.smartclient.com/showthread.php?p=91605#post91605
        isc.addProperties(type, OB.Utilities.Number.Grouping);
        // so can't define on field level 
        //      isc.addProperties(fld, OB.Utilities.Number.Grouping);
      }

      result.push(fld);
    }
    return result;
  }
});