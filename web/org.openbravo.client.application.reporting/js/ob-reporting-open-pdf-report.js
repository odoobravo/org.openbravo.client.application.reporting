/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

OB.Utilities.Action.set('OBUIREP_openPdfReport', function (paramObj) {
  var processParameters = paramObj.processParameters,
      params = isc.clone(processParameters);
  params._action = processParameters.actionHandler;
  params.mode = 'VALIDATE';
  params.processId = processParameters.processId;

  params.view = paramObj._processView;
  params.view.showProcessing(true);
  tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
  tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Executing', [params.view.tabTitle]));

  OB.RemoteCallManager.call(params._action, {}, params, function (resp, data, req) {
    if (data) {
      if ('true' === data.success) {
        params.mode = 'BUILD';
        params.type = 'PDF';
        OB.RemoteCallManager.call(params._action, {}, params, function (resp, data, req) {
          params.mode = 'DOWNLOAD';
          params.type = 'PDF';
          params.tmpfileName = data.tmpfileName;
          params.fileName = data.fileName;
          tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
          tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Done', [params.view.tabTitle]));
          params.view.showProcessing(false);
          //Copied from ob-parameter-window-view.js
          if (params.view.toolBarLayout) {
            for (i = 0; i < params.view.toolBarLayout.children.length; i++) {
              if (params.view.toolBarLayout.children[i].show) {
                params.view.toolBarLayout.children[i].show();
              }
            }
          }
          if (params.view.popupButtons) {
            params.view.popupButtons.show();
          }
          OB.Utilities.postThroughHiddenForm(OB.Application.contextUrl + 'org.openbravo.client.kernel', params);

        });
      } else if (data.error) {
        isc.warn(OB.I18N.getLabel(data.error));
        params.view.showProcessing(false);
        //Copied from ob-parameter-window-view.js
        if (params.view.toolBarLayout) {
          for (i = 0; i < params.view.toolBarLayout.children.length; i++) {
            if (params.view.toolBarLayout.children[i].show) {
              params.view.toolBarLayout.children[i].show();
            }
          }
        }
        if (params.view.popupButtons) {
          params.view.popupButtons.show();
        }
        tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
        tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Done', [params.view.tabTitle]));

      } else if (data.warning) {
        params.mode = 'BUILD';
        params.type = 'PDF';
        isc.ask(OB.I18N.getLabel(data.warning.message, data.warning.parameters), function (clickOK) {
          if (clickOK) {
            OB.RemoteCallManager.call(params._action, {}, params, function (resp, data, req) {
              params.mode = 'DOWNLOAD';
              params.type = 'PDF';
              params.tmpfileName = data.tmpfileName;
              params.fileName = data.fileName;
              tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
              tab.setTitle(OB.I18N.getLabel('OBUIAPP_ProcessTitle_Done', [params.view.tabTitle]));
              params.view.showProcessing(false);
              //Copied from ob-parameter-window-view.js
              if (params.view.toolBarLayout) {
                for (i = 0; i < params.view.toolBarLayout.children.length; i++) {
                  if (params.view.toolBarLayout.children[i].show) {
                    params.view.toolBarLayout.children[i].show();
                  }
                }
              }
              if (params.view.popupButtons) {
                params.view.popupButtons.show();
              }
              OB.Utilities.postThroughHiddenForm(OB.Application.contextUrl + 'org.openbravo.client.kernel', params);
            });
          } else {
            params.view.showProcessing(false);
            //Copied from ob-parameter-window-view.js
            if (params.view.toolBarLayout) {
              for (i = 0; i < params.view.toolBarLayout.children.length; i++) {
                if (params.view.toolBarLayout.children[i].show) {
                  params.view.toolBarLayout.children[i].show();
                }
              }
            }
            if (params.view.popupButtons) {
              params.view.popupButtons.show();
            }
            tab = OB.MainView.TabSet.getTab(params.view.viewTabId);
            tab.setTitle(params.view.tabTitle);
          }
        });
      }
    }

  });
});